'use strict';
require('v8-profiler');

var http = require('http'),
    https = require('https'),
    express = require('express'),
    path = require('path'),
    fs = require('fs'),
    mongoose = require('mongoose');

var localport = 3000;
var localurl  = "https://localhost";
/**
 * Main application file
 */

// Set default node environment to development
process.env.NODE_ENV = process.env.NODE_ENV || 'development';
process.env.AWS_TAG = "yedoit0e-21";
process.env.AWS_ID = "AKIAIQTPHUQXTNSMLJ5Q";
process.env.AWS_SECRET = "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";
process.env.AWS_DOMAIN = 'webservices.amazon.co.uk';

var config = require('./lib/config/config');
var db = mongoose.connect(config.mongo.uri, config.mongo.options);
																																																																														
// Bootstrap models
var modelsPath = path.join(__dirname, 'lib/models');
fs.readdirSync(modelsPath).forEach(function (file) {
  if (/(.*)\.(js$|coffee$)/.test(file)) {
    require(modelsPath + '/' + file);
  }
});

// Passport Configuration
var passport = require('./lib/config/passport');

// Setup Express
var options = {
    key: fs.readFileSync( './certs/yeahdone.it.key' ),
    cert: fs.readFileSync( './certs/yeahdone.it.crt' )
};

var app = express();

require('./lib/config/express')(app);
require('./lib/routes')(app);

var server = https.createServer(options, app).listen(config.port, config.ip, function () {
  console.log('Express server listening on %s:%d, in %s mode', config.ip, config.port, app.get('env'));
});

var insecApp = express();

var insecServer = http.createServer(insecApp).listen(localport, config.ip, function () {
  console.log('Express server listening on %s:%d, in %s mode', config.ip, localport, app.get('env'));
});

insecApp.get('*', function(req,res) {  
    res.redirect(localurl+req.url);
});

// Populate empty DB with sample data
// require('./lib/config/dummydata');

// Start server
/*app.listen(config.port, config.ip, function () {
  console.log('Express server listening on %s:%d, in %s mode', config.ip, config.port, app.get('env'));
});*/

// Expose app
exports = module.exports = app;
