'use strict';

var express = require('express'),
    path = require('path'),
    fs = require('fs'),
    mongoose = require('mongoose'),
    notification = require('./lib/controllers/notification');

/**
 * Main application file
 */

// Set default node environment to development
process.env.NODE_ENV = process.env.NODE_ENV || 'development';
process.env.AWS_TAG = "yedoit0e-21";
process.env.AWS_ID = "AKIAIQTPHUQXTNSMLJ5Q";
process.env.AWS_SECRET = "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";
process.env.AWS_DOMAIN = 'webservices.amazon.co.uk';

var config = require('./lib/config/config');
var db = mongoose.connect(config.mongo.uri, config.mongo.options);
																																																																														
// Bootstrap models
var modelsPath = path.join(__dirname, 'lib/models');
fs.readdirSync(modelsPath).forEach(function (file) {
  if (/(.*)\.(js$|coffee$)/.test(file)) {
    require(modelsPath + '/' + file);
  }
});

// Passport Configuration
var passport = require('./lib/config/passport');

// Setup Express
var app = express();
require('./lib/config/express')(app);
require('./lib/routes')(app);

// Populate empty DB with sample data
// require('./lib/config/dummydata');
console.log("Calling Notification...");
notification.sendSuggestionEmail("55472fe7e1b9cda84ad3632a");
console.log("Called Notification....");

// Start server
//app.listen(config.port, config.ip, function () {
//  console.log('Express server listening on %s:%d, in %s mode', config.ip, config.port, app.get('env'));
//});

// Expose app
// exports = module.exports = app;
