'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;
    
/**
 * Notification Schema
 */
var NotificationSchema = new Schema({
  _creator: { type: Schema.ObjectId, ref: 'User' },
  subject: String,
  content: String,
  _order: { type: Schema.ObjectId, ref: 'Order' },
  type: String,
  date: { type: Date, default: Date.now },
  read: { type: Boolean, default: false }
});

mongoose.model('Notification', NotificationSchema);
