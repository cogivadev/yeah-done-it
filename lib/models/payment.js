'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;
    
var PaymentSchema = new Schema({ 
	_order: { type: Schema.ObjectId, ref: 'Order' }, 
	_stripe: String
});

mongoose.model('Payment', PaymentSchema);
