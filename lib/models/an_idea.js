'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;
    
/**
 * Idea Schema
 */
var IdeaSchema = new Schema({ 
	name: String,
	date: Date,
	message: String
});

mongoose.model('Idea', IdeaSchema);
