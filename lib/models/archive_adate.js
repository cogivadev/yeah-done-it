'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;
    
/**
 * Archive Schema
 */
var ArchiveAdateSchema = new Schema({ 
	_creator: { type: Schema.ObjectId, ref: 'User' }, 
	_person: { type: Schema.ObjectId, ref: 'Person' },
	name: String,
	date: Date,
	message: String,
	status: String,
	budget: Number,
	createdate: { type: Date, default: Date.now }
});

mongoose.model('ArchiveAdate', ArchiveAdateSchema);
