'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;
    
var InviteSchema = new Schema({ 
	email: String,
	invite: String,
	createdate: { type: Date, default: Date.now } ,
	usedate: { type: Date, default: null }
});

mongoose.model('Invite', InviteSchema);
