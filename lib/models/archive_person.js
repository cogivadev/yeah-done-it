'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;
    
/**
 * Archive Schema
 */
var ArchivePersonSchema = new Schema({ 
  _creator: { type: Schema.ObjectId, ref: 'User' },
  name: String,
  address: String,
  preferences: String,
  createdate: { type: Date, default: Date.now }
});

mongoose.model('ArchivePerson', ArchivePersonSchema);
