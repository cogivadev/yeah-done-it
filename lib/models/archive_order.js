'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;
    
/**
 * Order Schema
 *
 * STATUS VALUES
 * 1 = SUGGESTED
 * 2 = APPROVED
 * 3 = ORDERED
 * 9 = FAVOURITE
 * 0 = REJECTED
 */

var ArchiveOrderSchema = new Schema({
    _user: { type: Schema.ObjectId, ref: 'User' },
	  _person: { type: Schema.ObjectId, ref: 'Person' },
	  _adate: { type: Schema.ObjectId, ref: 'Adate' },
    datedate: Date,
  	datesent: Date,
  	price: String,
  	status: Number,
  	details: {
  		itemId: Number,
  		pageUrl: String,
  		tinyImage: String,
  		mediumImage: String,
  		largeImage: String,
        listPrice: {
            Amount: String,
            CurrencyCode: String,
            FormattedPrice: String
        },
        title: String,
        productGroup: String
  	},
    payment: {
      date: Date,      
      pid: String,
      card: String,
    }
});

mongoose.model('ArchiveOrder', ArchiveOrderSchema);