'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema
/**
 * Person Schema
 */
var PersonSchema = new Schema({
  _creator: { type: Schema.ObjectId, ref: 'User' },
  name: String,
  address: String,
  preferences: String,
  gender: String,
  age: String,
  createdate: { type: Date, default: Date.now }
});

mongoose.model('Person', PersonSchema);
