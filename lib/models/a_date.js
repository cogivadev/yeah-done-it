'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;
    
/**
 * Date Schema
 *
 * STATUS VALUES
 * 1 = HAS SUGGESTION
 * 2 = HAS APPROVAL
 * 0 = WAITING FOR ACTION
 */
var DateSchema = new Schema({ 
	_creator: { type: Schema.ObjectId, ref: 'User' }, 
	_person: { type: Schema.ObjectId, ref: 'Person' },
	name: String,
	date: Date,
	message: String,
	status: String,
	budget: Number,
	createdate: { type: Date, default: Date.now }
});

mongoose.model('Adate', DateSchema);
