'use strict';

var person = require('./controllers/person'),
    adate = require('./controllers/adate'),
    order = require('./controllers/order'),
    email = require('./controllers/email'),
    payment = require('./controllers/payment'),
    apa = require('./controllers/apa'),
    notification = require('./controllers/notification'),
    index = require('./controllers'),
    users = require('./controllers/users'),
    session = require('./controllers/session'),
    middleware = require('./middleware'),
    forceUrl = require('express-force-domain'),
    passport = require('passport'),    
    site_url = 'https://localhost'; 
    //site_url = 'https://yeahdone.it'; 
 
/*
 * Application routes
 */
module.exports = function(app) {

  // Check Domain and Redirect
  //app.use( forceUrl(site_url) );

  // Server API Routes

  // PERSON ROUTES
  {
    app.route('/api/allpersons')
      .get(middleware.authadmin, person.AllPersons);
    app.route('/api/mypersons')
      .get(middleware.auth, person.MyPersons);
    app.route('/api/person/:id')
      .get(middleware.auth, person.APerson)
      .put(middleware.auth, person.PersonSave)
      .delete(middleware.auth, person.PersonDelete);
    app.route('/api/person')
      .post(middleware.auth, person.PersonCreate);
  }

  // AMAZON ROUTES
  {
    app.route('/api/amazonitem/:id')
      .get(apa.getAmazonItem);
    app.route('/api/amazonsearch/:searchterm')
      .post(apa.searchAmazon);    
    app.route('/api/amazonsimilaritem/:id')
      .get(apa.getAmazonSimilarItems);
  }
  
  // ADATE routes
  {
    app.route('/api/alladates')
      .get(middleware.authadmin, adate.AllAdates);
    //app.route('/api/comingadates')
    //  .get(middleware.authadmin, adate.ComingAdates);
    app.route('/api/comingadates')
      .get(middleware.authadmin, adate.ComingAdates);
    app.route('/api/personadates/:person')
      .get(middleware.auth, adate.PersonAdates);
    app.route('/api/adate/:id')
      .get(middleware.auth, adate.AAdate)
      .put(middleware.auth, adate.AdateSave)
      .delete(middleware.auth, adate.AdateDelete);
    app.route('/api/adate')
      .post(middleware.auth, adate.AdateCreate);
  }

  // Notification and email routes
  app.route('/api/notifications')
    .get(notification.MyNotifications);
  app.route('/api/notifications/read/:id')
    .get(notification.MarkAsRead);    
  app.route('/api/notificationcount')
    .get(notification.MyNotificationsCount);
  app.route('/api/bugreport/')
    .post(middleware.auth, email.sendBugEmail);
  app.route('/api/sendcontact/')
    .post(middleware.auth, email.sendContactEmail);
  app.route('/api/sendinvite/')
    .post(middleware.authadmin, email.sendInviteEmail);

  // ORDER ROUTES
  app.route('/api/personorders/:person')
    .get(order.getPersonOrders);
  app.route('/api/order/')
    .post(middleware.authadmin, order.OrderCreate);
  app.route('/api/userorders/')
    .get(middleware.auth, order.getUserOrders);
  app.route('/api/order/remind/:id')
    .post(middleware.authadmin, order.OrderRemind);
  app.route('/api/chargecard/:id')
    .post(middleware.auth, payment.chargeCard)
  app.route('/api/order/:id')
    .post(middleware.auth, order.UpdateStatus)
    .delete(middleware.authadmin, order.OrderDelete);

  // USER Routes
  app.route('/api/users') 
    .get(middleware.authadmin, users.getAll) 
    .post(users.create)
    .put(users.changePassword);
  app.route('/api/users/me')
    .get(middleware.auth, users.me);
  app.route('/api/users/:id')
    .get(middleware.auth, users.show);
  app.route('/api/checkinvite/:id')
    .get(users.checkInvite);

  // SESSION Routes
  app.route('/api/session')
    .post(session.login)
    .delete(session.logout);

  app.route('/api/facebook')
    //.get(session.loginfb);
    .get(passport.authenticate('facebook', { scope : 'email' }));
  app.route('/api/facebook/callback')
    .get(session.loginfbcb);

  // All undefined api routes should return a 404
  app.route('/api/*')
    .get(function(req, res) {
      res.send(404);
    });

  // All other routes to use Angular routing in app/scripts/app.js
  app.route('/partials/*')
    .get(index.partials);
  app.route('/F8D43E081329D113F1D75E1CEAC660DC.txt')
    .get(index.sslcerts);
  app.route('/*')
    .get(middleware.setUserCookie, index.index);

};