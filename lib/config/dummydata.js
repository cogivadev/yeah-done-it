'use strict';

var mongoose = require('mongoose'), 
  User = mongoose.model('User'),
  Person = mongoose.model('Person'),
  Adate =  mongoose.model('Adate'),
  Order =  mongoose.model('Order'),
  Notification = mongoose.model('Notification');

/**
 * Populate database with sample application data
 */

/* Clear old users, then add a default user */

console.log("Adding Dummy Data");

var am_details = {
  itemId: "0747591059",
  pageUrl: "http://www.amazon.co.uk/Potter-Deathly-Hallows-Childrens-Edition/dp/0747591059%3FSubscriptionId%3DAKIAIQTPHUQXTNSMLJ5Q%26tag%3Dyedoit0e-21%26linkCode%3Dxm2%26camp%3D2025%26creative%3D165953%26creativeASIN%3D0747591059",
  tinyImage: "http://ecx.images-amazon.com/images/I/51tB0kftR-L._SL75_.jpg",
  mediumImage: "http://ecx.images-amazon.com/images/I/51tB0kftR-L._SL160_.jpg",
  largeImage: "http://ecx.images-amazon.com/images/I/51tB0kftR-L.jpg",
  listPrice: {
      Amount: "1799",
      CurrencyCode: "GBP",
      FormattedPrice: "&pound;17.99"
  },
  title: "Three Harry Potter and the Deathly Hallows",
  productGroup: "Book"
};

//Clear old things, then add things in
Order.find({}).remove(function(){
  Adate.find({}).remove(function(){
    Person.find({}).remove(function() {

      User.find({}).remove(function() {
        User.create({
          provider: 'local',
          name: 'Ben Drury',
          email: 'ben@cogiva.com',
          password: 'test',
          role: 'admin',
        }, function(err, u2) {
            Person.create( {
                _creator: u2._id,
                name : 'Shelley Drury',
                address : '8 Oxford Street, Whitley Bay, NE26 1AE'
              }, function(err, newPerson) {
                  Adate.create ({
                      _person: newPerson._id,
                      _creator: u2._id,
                      name: "Birthday!",
                      status: 1,
                      date: new Date('2015', '04', '15'),
                      message: "Happy Birthday!"
                  }, function(err, newAdate) {
                    am_details.title = "This is order item One"
                    Order.create({
                      _person: newPerson._id,
                      _adate: newAdate._id,
                      datesent: new Date('2014', '11', '17'),
                      price: "£17.99",
                      status: 1,
                      details: am_details
                    }, function() {                      
                    });
                    am_details.title = "This is order item Two"
                    Order.create({
                      _person: newPerson._id,
                      _adate: newAdate._id,
                      datesent: new Date('2014', '11', '17'),
                      price: "£14.99",
                      status: 2,
                      details: am_details
                    }, function() {                      
                    });
                    am_details.title = "This is order item Three"
                    Order.create({
                      _person: newPerson._id,
                      _adate: newAdate._id,
                      datesent: new Date('2014', '11', '17'),
                      price: "£17.99",
                      status: 3,
                      details: am_details
                    }, function() {                      
                    });
                    am_details.title = "This is order item Four"
                    Order.create({
                      _person: newPerson._id,
                      _adate: newAdate._id,
                      datesent: new Date('2014', '11', '17'),
                      price: "£10.99",
                      status: 0,
                      details: am_details
                    }, function() {                      
                    });
                  });
                  Adate.create ({
                      _person: newPerson._id,
                      _creator: u2._id,
                      name: "Anniversary!",
                      status: 0,
                      date: new Date('2015', '04', '10'),
                      message: "I Love You?"
                  }, function(err, newAdate) {
                    am_details.title = "This is order item five"
                    Order.create({
                      _person: newPerson._id,
                      _adate: newAdate._id,
                      datesent: new Date('2014', '11', '17'),
                      price: "£6.99",
                      status: 1,
                      details: am_details
                    }, function() {                      
                    });
                    am_details.title = "This is order item six"
                    Order.create({
                      _person: newPerson._id,
                      _adate: newAdate._id,
                      datesent: new Date('2014', '11', '17'),
                      price: "£6.99",
                      status: 2,
                      details: am_details
                    }, function() {                      
                    });
                    am_details.title = "This is order item seven"
                    Order.create({
                      _person: newPerson._id,
                      _adate: newAdate._id,
                      datesent: new Date('2014', '11', '17'),
                      price: "£7.99",
                      status: 3,
                      details: am_details
                    }, function() {                      
                    });
                    am_details.title = "This is order item eight"
                    Order.create({
                      _person: newPerson._id,
                      _adate: newAdate._id,
                      datesent: new Date('2014', '11', '17'),
                      price: "£8.99",
                      status: 0,
                      details: am_details
                    }, function() {                      
                    });
                  });
              });
              Person.create({
                _creator: u2._id,
                name : 'Mary Pearson',
                address : '7 John Turner Road, Darley Dale, DE4 7TR'
              }, function(err, newPerson) {
                  Adate.create ({
                      _person: newPerson._id,
                      _creator: u2._id,
                      name: "Birthday!",
                      status: 1,
                      date: new Date('2015', '04', '15'),
                      message: "Have a fabulous birthday"
                  }, function(err, newAdate) {

                  });
                  Adate.create ({
                      _person: newPerson._id,
                      _creator: u2._id,
                      name: "Retirement Day!",
                      status: 2,
                      date: new Date('2015', '05', '01'),
                      message: "Congratulations!"
                  }, function(err, newAdate) {

                  });
                  Adate.create ({
                      _person: newPerson._id,
                      _creator: u2._id,
                      name: "Official Birthday!",
                      status: 0,
                      date: new Date('2015', '05', '01'),
                      message: "Another One?"
                  }, function(err, newAdate) {

                  });                
              });
              Person.create( {
                _creator: u2._id,
                name : 'David Drury',
                address : '1 Waterford Land, Cherry Willingham, LN3 4AL'    
              }, function(err, newPerson) {
                  Adate.create ({
                      _person: newPerson._id,
                      _creator: u2._id,
                      name: "Birthday!",
                      status: 1,
                      date: new Date('2015', '04', '15'),
                      message: "Have a fabulous birthday"
                  }, function(err, newAdate) {

                  });
                  Adate.create ({
                      _person: newPerson._id,
                      _creator: u2._id,
                      name: "Retirement Day!",
                      status: 2,
                      date: new Date('2015', '05', '01'),
                      message: "Congratulations!"
                  }, function(err, newAdate) {

                  });
                  Adate.create ({
                      _person: newPerson._id,
                      _creator: u2._id,
                      name: "Official Birthday!",
                      status: 0,
                      date: new Date('2015', '05', '01'),
                      message: "Another One?"
                  }, function(err, newAdate) {

                  });
              });
              Person.create( {
                _creator: u2._id,
                name : 'Isabel Drury',
                address : '10 York Road, Whitley Bay, NE26 1AB'
              }, function(err, newPerson) {
                  Adate.create ({
                      _person: newPerson._id,
                      _creator: u2._id,
                      name: "Birthday!",
                      date: new Date('2016', '04', '17'),
                      message: "Happy Birthday?"
                  }, function(err, newAdate) {

                  });         
                  Adate.create ({
                      _person: newPerson._id,
                      _creator: u2._id,
                      name: "Birthday!",
                      status: 1,
                      date: new Date('2015', '04', '15'),
                      message: "Have a fabulous birthday"
                  }, function(err, newAdate) {

                  });
                  Adate.create ({
                      _person: newPerson._id,
                      _creator: u2._id,
                      name: "Retirement Day!",
                      status: 2,
                      date: new Date('2015', '05', '01'),
                      message: "Congratulations!"
                  }, function(err, newAdate) {

                  });
                  Adate.create ({
                      _person: newPerson._id,
                      _creator: u2._id,
                      name: "Official Birthday!",
                      status: 0,
                      date: new Date('2015', '05', '01'),
                      message: "Another One?"
                  }, function(err, newAdate) {

                  });              });     
            console.log('finished populating user ben');
          }
        );
      });                     

    });
  });
});

// Clear old notifications and replace with dummy data
Notification.find({}).remove(function() {
  /*Notification.create({
    _creator: "54679200502e901427fec6df",
    _Person: "54679200502e901427fec6de",
    subject: "This is the first Notification!",
    content: "There is some Lorum ipsum stuff in here - look:  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce sit amet tincidunt lorem. Mauris posuere venenatis urna sed fringilla. Proin pretium aliquet pretium. Suspendisse sit amet nulla enim. Nunc sollicitudin et leo non congue. Aliquam diam eros, fermentum at nisl nec, ullamcorper elementum nibh. Phasellus risus lectus, sodales et nulla at, maximus suscipit leo. Suspendisse laoreet in quam ac euismod. Duis ultricies efficitur ipsum sed rutrum. In ullamcorper vel risus id lobortis. Curabitur malesuada id ex vel auctor. Integer ullamcorper vel ante eu mattis. Curabitur gravida tortor at justo porttitor molestie. Phasellus cursus tortor sapien, sit amet rutrum purus faucibus ac.",
    type: "General",
    date: new Date()
  });
  Notification.create({
    _creator: "54679200502e901427fec6df",
    _Person: "54679200502e901427fec6de",
    subject: "This is the Second Notification!",
    content: "There is some Lorum ipsum stuff in here - look:  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce sit amet tincidunt lorem. Mauris posuere venenatis urna sed fringilla. Proin pretium aliquet pretium. Suspendisse sit amet nulla enim. Nunc sollicitudin et leo non congue. Aliquam diam eros, fermentum at nisl nec, ullamcorper elementum nibh. Phasellus risus lectus, sodales et nulla at, maximus suscipit leo. Suspendisse laoreet in quam ac euismod. Duis ultricies efficitur ipsum sed rutrum. In ullamcorper vel risus id lobortis. Curabitur malesuada id ex vel auctor. Integer ullamcorper vel ante eu mattis. Curabitur gravida tortor at justo porttitor molestie. Phasellus cursus tortor sapien, sit amet rutrum purus faucibus ac.",
    type: "General",
    date: new Date()
  });
  Notification.create({
    _creator: "54679200502e901427fec6df",
    _Person: "54679200502e901427fec6de",
    subject: "This is the third Notification!",
    content: "There is some Lorum ipsum stuff in here - look:  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce sit amet tincidunt lorem. Mauris posuere venenatis urna sed fringilla. Proin pretium aliquet pretium. Suspendisse sit amet nulla enim. Nunc sollicitudin et leo non congue. Aliquam diam eros, fermentum at nisl nec, ullamcorper elementum nibh. Phasellus risus lectus, sodales et nulla at, maximus suscipit leo. Suspendisse laoreet in quam ac euismod. Duis ultricies efficitur ipsum sed rutrum. In ullamcorper vel risus id lobortis. Curabitur malesuada id ex vel auctor. Integer ullamcorper vel ante eu mattis. Curabitur gravida tortor at justo porttitor molestie. Phasellus cursus tortor sapien, sit amet rutrum purus faucibus ac.",
    type: "General",
    date: new Date()
  });
  Notification.create({
    _creator: "54679200502e901427fec6df",
    _Person: "54679200502e901427fec6de",
    subject: "This is the 4th Notification!",
    content: "There is some Lorum ipsum stuff in here - look:  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce sit amet tincidunt lorem. Mauris posuere venenatis urna sed fringilla. Proin pretium aliquet pretium. Suspendisse sit amet nulla enim. Nunc sollicitudin et leo non congue. Aliquam diam eros, fermentum at nisl nec, ullamcorper elementum nibh. Phasellus risus lectus, sodales et nulla at, maximus suscipit leo. Suspendisse laoreet in quam ac euismod. Duis ultricies efficitur ipsum sed rutrum. In ullamcorper vel risus id lobortis. Curabitur malesuada id ex vel auctor. Integer ullamcorper vel ante eu mattis. Curabitur gravida tortor at justo porttitor molestie. Phasellus cursus tortor sapien, sit amet rutrum purus faucibus ac.",
    type: "General",
    date: new Date()
  });
  Notification.create({
    _creator: "54679200502e901427fec6de",
    _Person: "54679200502e901427fec6df",
    subject: "This is the first Notification!",
    content: "There is some Lorum ipsum stuff in here - look:  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce sit amet tincidunt lorem. Mauris posuere venenatis urna sed fringilla. Proin pretium aliquet pretium. Suspendisse sit amet nulla enim. Nunc sollicitudin et leo non congue. Aliquam diam eros, fermentum at nisl nec, ullamcorper elementum nibh. Phasellus risus lectus, sodales et nulla at, maximus suscipit leo. Suspendisse laoreet in quam ac euismod. Duis ultricies efficitur ipsum sed rutrum. In ullamcorper vel risus id lobortis. Curabitur malesuada id ex vel auctor. Integer ullamcorper vel ante eu mattis. Curabitur gravida tortor at justo porttitor molestie. Phasellus cursus tortor sapien, sit amet rutrum purus faucibus ac.",
    type: "General",
    date: new Date()
  });
  Notification.create({
    _creator: "54679200502e901427fec6de",
    _Person: "54679200502e901427fec6df",
    subject: "This is the second Notification!",
    content: "There is some Lorum ipsum stuff in here - look:  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce sit amet tincidunt lorem. Mauris posuere venenatis urna sed fringilla. Proin pretium aliquet pretium. Suspendisse sit amet nulla enim. Nunc sollicitudin et leo non congue. Aliquam diam eros, fermentum at nisl nec, ullamcorper elementum nibh. Phasellus risus lectus, sodales et nulla at, maximus suscipit leo. Suspendisse laoreet in quam ac euismod. Duis ultricies efficitur ipsum sed rutrum. In ullamcorper vel risus id lobortis. Curabitur malesuada id ex vel auctor. Integer ullamcorper vel ante eu mattis. Curabitur gravida tortor at justo porttitor molestie. Phasellus cursus tortor sapien, sit amet rutrum purus faucibus ac.",
    type: "General",
    date: new Date()
  });*/
});
