'use strict';

var fs = require('fs'),
    path = require('path');

/**
 * Send partial, or 404 if it doesn't exist
 */
exports.partials = function(req, res) {
  var stripped = req.url.split('.')[0];
  var requestedView = path.join('./', stripped);
  res.render(requestedView, function(err, html) {
    if(err) {
      console.log("Error rendering partial '" + requestedView + "'\n", err);
      res.status(404);
      res.send(404);
    } else {
      res.send(html);
    }
  });
};

/**
 * Send our single page app
 */
exports.index = function(req, res) {
  res.render('index');
};

exports.sslcerts = function(req, res) {
    fs.readFile('./lib/F8D43E081329D113F1D75E1CEAC660DC.txt', 'utf8', function (err, contents) {
      res.send(contents);
    });
    //res.sendfile('./lib/F8D43E081329D113F1D75E1CEAC660DC.txt');
};
