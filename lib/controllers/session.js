'use strict';

var mongoose = require('mongoose'),
    passport = require('passport');

/**
 * Logout
 */
exports.logout = function (req, res) {
  req.logout();
  res.send(200);
};

/**
 * Login
 */
exports.login = function (req, res, next) {
  console.log(next);
  passport.authenticate('local', function(err, user, info) {
    var error = err || info;
    if (error) return res.json(401, error);

    req.logIn(user, function(err) {    
      if (err) return res.send(err);
      res.json(req.user.userInfo);
    });
  })(req, res, next);
};

/**
 * Login
 */
exports.loginfb = function (req, res, next) {
  console.log(next);
  console.log("in the session...");
  passport.authenticate('facebook', { scope : 'email' });
  console.log("After the call ...");
};

exports.loginfbcb = function(req, res, next) {
  console.log("in FB Callback...");
  passport.authenticate('facebook', { failureRedirect: '/' }, function(err, user, info) {   
    var error = err || info;
    if (error) return res.json(401, error);

    req.logIn(user, function(err) {    
      if (err) return res.send(err);
      res.json(req.user.userInfo);
    });
  })(req, res, next);
};