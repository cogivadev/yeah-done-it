var fs = require('fs'),
	mongoose = require('mongoose'),
	Invites = mongoose.model('Invite'),
	nodemailer = require("nodemailer");

// create reusable transport method (opens pool of SMTP connections)
var smtpTransport = nodemailer.createTransport("SMTP",{
    service: "Gmail",
    auth: {
        user: "ben@cogiva.com",
        pass: "zmzbzeuoszbcnzrv"
    }
});

var pidGenerator = function() {
	var result = '';
	var chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	var length = 20;
    for (var i = length; i > 0; --i) result += chars[Math.round(Math.random() * (chars.length - 1))];
    return result;
};

var addinviteToDB = function(email, invite, callback) {
	Invites.find({ email: email }, function(err, docs){
		if (err || docs.length > 0 ) {
		    console.log("Err/docs...", err);
		    console.log("Err/docs...", docs);
        	if (callback) callback(false);
		} else {
		    console.log("Err/docs YES...", docs);
			var newInvite = {
				email: email,
				invite: invite
			};

			Invites.create(newInvite, function(err, doc) {
		        if (err) {
		        	console.log(err);
		        	if (callback) callback(false);
		        }
		        console.log("In added invite...");
		        if (callback) callback(true);
		    });	

	    }	
	});
};

var sendEmail = function(to, subject, text, html, callback, bugemail) {
	fs.readFile('./lib/mails/email-header.html', 'utf8', function (err, eheader) {
	  	if (err) {
			console.log("File error");
		    console.log(err);
		    callback(err);
		}							
		fs.readFile('./lib/mails/email-footer.html', 'utf8', function (err, efooter) {
		  	if (err) {
				console.log("File error");
			    console.log(err);
			    callback(err);
			}	
			if (!bugemail) {
				text = text + efooter;
				html = eheader + html + efooter;				
			} else {
				text = text;
				html = html;
			}		

			var mailOptions = {
			    from: 'Yeah Done It! <admin@yeahdone.it>', // sender address 
			    to: to, // list of receivers 
			    subject: subject, // Subject line 
			    text: text, // plaintext body 
			    html: html // html body 
			};
			
			// send mail with defined transport object 
			smtpTransport.sendMail(mailOptions, function(error, info){
			    if(error){
					console.log("Transport error");
			        console.log(error);
					callback(error);
			    }else{
			    	callback(info);
			    }
			});				
		});							
	});
};

exports.sendSuggestionEmail = function(orderid, finalorder, callback) {
    var allText = "";					

	fs.readFile('./lib/mails/suggestemail.html', 'utf8', function (err, econtent) {
	  	if (err) {
			console.log("File error");
		    console.log(err);
		    callback(err, null);
		}

		allText = econtent;
		var to = finalorder._adate._creator.email;
		var un = finalorder._adate._creator.name;
		var address = finalorder._person.name + ", " + finalorder._person.address;
		var message =  finalorder._adate.message;
		var title = finalorder.details.title;;
		var price = finalorder.price;
		var img = finalorder.details.tinyImage;
		var thedate = finalorder._adate.name;
		var link = finalorder.details.pageUrl;
		var id = finalorder._id;

		allText = allText.replace("$$USERNAME", un).replace("$$ADDRESS", address).replace("$$MESSAGE", message).replace("$$DATE", thedate).replace("$$TITLE", title).replace("$$PRICE", price).replace("$$IMGSRC", img).replace("$$AMZLINK", link).replace("$$THEID",id).replace("$$THEID",id);
		sendEmail(to, 'We have a suggestion for you', allText, allText, function (info) {
		    callback(null, info);
		});														
	});
};

exports.sendReminderEmail = function(orderid, finalorder, callback) {
    var allText = "";					

	fs.readFile('./lib/mails/remindemail.html', 'utf8', function (err, econtent) {
	  	if (err) {
			console.log("File error");
		    console.log(err);
		    callback(err, null);
		}

		allText = econtent;
		var to = finalorder._adate._creator.email;
		var un = finalorder._adate._creator.name;
		var address = finalorder._person.name + ", " + finalorder._person.address;
		var message =  finalorder._adate.message;
		var title = finalorder.details.title;;
		var price = finalorder.price;
		var img = finalorder.details.tinyImage;
		var thedate = finalorder._adate.name;
		var link = finalorder.details.pageUrl;
		var id = finalorder._id;

		allText = allText.replace("$$USERNAME", un).replace("$$ADDRESS", address).replace("$$MESSAGE", message).replace("$$DATE", thedate).replace("$$TITLE", title).replace("$$PRICE", price).replace("$$IMGSRC", img).replace("$$AMZLINK", link).replace("$$THEID",id).replace("$$THEID",id);
		sendEmail(to, 'Reminder: There\'s a suggestion awaiting your approval', allText, allText, function (info) {
		    callback(null, info);
		});														
	});
};

exports.sendRejectionEmail = function(orderid, finalorder, callback) {
    var allText = "";					

	fs.readFile('./lib/mails/rejectemail.html', 'utf8', function (err, econtent) {
	  	if (err) {
			console.log("File error");
		    console.log(err);
		    callback(err, null);
		}

		allText = econtent;
		var to = finalorder._adate._creator.email;
		var un = finalorder._adate._creator.name;
		var address = finalorder._person.name + ", " + finalorder._person.address;
		var message =  finalorder._adate.message;
		var title = finalorder.details.title;;
		var price = finalorder.price;
		var img = finalorder.details.tinyImage;
		var thedate = finalorder._adate.name;
		var link = finalorder.details.pageUrl;
		var id = finalorder._id;

		allText = allText.replace("$$USERNAME", un).replace("$$ADDRESS", address).replace("$$MESSAGE", message).replace("$$DATE", thedate).replace("$$TITLE", title).replace("$$PRICE", price).replace("$$IMGSRC", img).replace("$$AMZLINK", link).replace("$$THEID",id).replace("$$THEID",id);
		sendEmail(to, 'Confirmation: You rejected a YDI suggestion', allText, allText, function (info) {
		    callback(null, info);
		});														
	});
};

exports.sendPaymentEmail = function(orderid, finalorder, callback) {
    var allText = "";					

	fs.readFile('./lib/mails/sentemail.html', 'utf8', function (err, econtent) {
	  	if (err) {
			console.log("File error");
		    console.log(err);
		    callback(err, null);
		}

		allText = econtent;
		var to = finalorder._adate._creator.email;
		var un = finalorder._adate._creator.name;
		var address = finalorder._person.name + ", " + finalorder._person.address;
		var message =  finalorder._adate.message;
		var title = finalorder.details.title;;
		var price = finalorder.price;
		var img = finalorder.details.tinyImage;
		var thedate = finalorder._adate.name;
		var link = finalorder.details.pageUrl;
		var id = finalorder._id;

		allText = allText.replace("$$USERNAME", un).replace("$$ADDRESS", address).replace("$$MESSAGE", message).replace("$$DATE", thedate).replace("$$TITLE", title).replace("$$PRICE", price).replace("$$IMGSRC", img).replace("$$AMZLINK", link).replace("$$THEID",id).replace("$$THEID",id);
		sendEmail(to, 'Confirmation: Your gift has been sent.', allText, allText, function (info) {
		    callback(null, info);
		});														
	});
};

exports.sendSentEmail = function(orderid, finalorder, callback) {
    var allText = "";					

	fs.readFile('./lib/mails/sentemail.html', 'utf8', function (err, econtent) {
	  	if (err) {
			console.log("File error");
		    console.log(err);
		    callback(err, null);
		}

		allText = econtent;
		var to = finalorder._adate._creator.email;
		var un = finalorder._adate._creator.name;
		var address = finalorder._person.name + ", " + finalorder._person.address;
		var message =  finalorder._adate.message;
		var title = finalorder.details.title;;
		var price = finalorder.price;
		var img = finalorder.details.tinyImage;
		var thedate = finalorder._adate.name;
		var link = finalorder.details.pageUrl;
		var id = finalorder._id;

		allText = allText.replace("$$USERNAME", un).replace("$$ADDRESS", address).replace("$$MESSAGE", message).replace("$$DATE", thedate).replace("$$TITLE", title).replace("$$PRICE", price).replace("$$IMGSRC", img).replace("$$AMZLINK", link).replace("$$THEID",id).replace("$$THEID",id);
		sendEmail(to, 'Confirmation: We\'ve sent your approved suggestion', allText, allText, function (info) {
		    callback(null, info);
		});														
	});
};

exports.sendApprovedEmail = function(orderid, finalorder, callback) {
    var allText = "";					

	fs.readFile('./lib/mails/approveemail.html', 'utf8', function (err, econtent) {
	  	if (err) {
			console.log("File error");
		    console.log(err);
		    callback(err, null);
		}

		allText = econtent;
		var to = finalorder._adate._creator.email;
		var un = finalorder._adate._creator.name;
		var address = finalorder._person.name + ", " + finalorder._person.address;
		var message =  finalorder._adate.message;
		var title = finalorder.details.title;;
		var price = finalorder.price;
		var img = finalorder.details.tinyImage;
		var thedate = finalorder._adate.name;
		var link = finalorder.details.pageUrl;
		var id = finalorder._id;

		allText = allText.replace("$$USERNAME", un).replace("$$ADDRESS", address).replace("$$MESSAGE", message).replace("$$DATE", thedate).replace("$$TITLE", title).replace("$$PRICE", price).replace("$$IMGSRC", img).replace("$$AMZLINK", link).replace("$$THEID",id).replace("$$THEID",id);
		sendEmail(to, 'Confirmation: You approved a YDI suggestion', allText, allText, function (info) {
		    callback(null, info);
		});														
	});
};

exports.sendBugEmail = function(req, res) {
    var allText = "<b>Page:</b> " + req.body.page + "<br /><b>Reported By:</b>" + req.body.email + "<br /><br />" + req.body.content;					
	var to = "wrike@wrike.com, info@cogiva.com";

	sendEmail(to, "YDI BUG: " + req.body.title, allText, allText, function (info) {
	    return res.send(200);
	}, true);														
};

exports.sendContactEmail = function(req, res) {
    var allText = "<b>Message From:</b>" + req.body.email + "<br /><br />" + req.body.content;					
	var to = "wrike@wrike.com, info@cogiva.com";

	sendEmail(to, "YDI Message: " + req.body.title, allText, allText, function (info) {
	    return res.send(200);
	}, true);														
};

exports.sendInviteEmail = function(req, res) {
	console.log("SIE: sending..", req.body);
    var allText = "";					
	var to = req.body.email;
	var usinvite = pidGenerator();
	var link = "http://yeahdone.it/signup?email=" + to + "&invite=" + usinvite;
	
	addinviteToDB(to, usinvite, function(result){
		console.log("SIE: Added to DB..");
		if (result) {
			console.log("True return..");
			fs.readFile('./lib/mails/inviteemail.html', 'utf8', function (err, econtent) {
				console.log("File Read");
			  	if (err) {
					console.log("File error");
				    console.log(err);
					return res.send(400);
				}

				allText = econtent;
				allText = allText.replace("$$INVITE", usinvite).replace("$$EMAIL", to).replace("$$LINK", link).replace("$$LINK2", link);
				sendEmail(to, 'Yeah, you\'re invited!', allText, allText, function (info) {
					console.log("SIE: Emailed..");
					return res.send(200);
				});														
			});		
		} else {
			console.log("false return..");
			return res.send(400);
		}
				
	});

													
};