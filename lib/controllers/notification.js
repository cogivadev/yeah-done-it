'use strict';

var mongoose = require('mongoose'),
    Notification = mongoose.model('Notification'),
    User = mongoose.model('User'),
    Order = mongoose.model('Order'),
	fs = require('fs'),
	email = require('./email');

/**
 * Get notifications
 */
exports.MyNotifications = function(req, res) {
	if (req.user){
		var userid = req.user._id;
		return Notification.find( { _creator: userid, read: false } ).sort('-date').populate('_order').exec(function (err, Notifications) {
			if (!err) {
				Notification.populate(Notifications, {path: '_order._person', model: 'Person'}, function(err, NotificationsPlus){
					if (!err) { 
						Notification.populate(NotificationsPlus, {path: '_order._adate', model: 'Adate'}, function(err, NotificationsPlusPlus){
							if (!err) { 
								return res.json(NotificationsPlusPlus);
							} else {
								return res.send(err);						
							}
						});					
					} else {
						return res.send(err);						
					}
				});
			} else {
				return res.send(err);
			}
		});
	} else {
		return null;
	}
};  

exports.MarkAsRead = function(req, res){
	var readUpdate = { read: true };
	Notification.findByIdAndUpdate(req.params.id, readUpdate, function (err, doc) {
        if (err) {
        	console.log(err);
        	return res.send(400);
        }
    	res.status(200).send(doc);
    });
};

exports.MyNotificationsCount = function(req, res) {
	if (req.user){
		var userid = req.user._id;
		return Notification.count( { _creator: userid, read: false }, function (err, Notifications) {
			if (!err) {
				return res.json(Notifications);
			} else{
				return res.send(err);
			}
		});
	} else {
		return null;
	}	
};

exports.NotificationRemind = function(orderid, callback) {	
	// Create Notification
	return Order.find( { _id: orderid }).populate('_adate').populate('_person').exec(function (err, orders) {
		if (!err) {
			try{
				Order.populate(orders,{path: '_adate._creator', model: 'User'}, function(err, finalorders){
					if (!err) {
		    			email.sendReminderEmail(orderid, finalorders[0], callback);
		    			callback(null, finalorders[0])
					} else {
						console.log("populate error");
					    callback(err, null);
					}
				});
			} catch (e) {
				console.log("We've got an order with null adate!");
			}
		} else{
		    callback(err, null);
		}
	});    
};

exports.NotificationCreate = function(status, orderid, callback) {	
	// Create Notification
	return Order.find( { _id: orderid }).populate('_adate').populate('_person').exec(function (err, orders) {
		if (!err) {
			try {
				Order.populate(orders,{path: '_adate._creator', model: 'User'}, function(err, finalorders){
					if (!err) {
					    switch (status) {
					    	case 0:
								var newNotification = new Notification({
									_creator: finalorders[0]._adate._creator._id,
									subject: "Order Rejection",
									content: "You rejected a suggested order.",
									_order: orderid,
									type: "reject",
									date: { type: Date, default: Date.now }
								});

							    Notification.create(newNotification, function(err, notification) {
							        if (err) {
							        	console.log(err);
							        	callback(err, null);
							        }
					    			email.sendRejectionEmail(orderid, finalorders[0], callback);
							    });
					    		break;
					    	case 1:
								var newNotification = new Notification({
									_creator: finalorders[0]._adate._creator._id,
									subject: "Order Suggestion",
									content: "An new order has been suggested.",
									_order: orderid,
									type: "suggest",
									date: { type: Date, default: Date.now }
								});
							    Notification.create(newNotification, function(err, notification) {
							        if (err) {
							        	console.log(err);
							        	callback(err, null);
							        }
					    			email.sendSuggestionEmail(orderid, finalorders[0], callback);
							    });
					    		break;
					    	case 2:
								var newNotification = new Notification({
									_creator: finalorders[0]._adate._creator._id,
									subject: "Order Approval",
									content: "You have approved a new order.",
									_order: orderid,
									type: "approve",
									date: { type: Date, default: Date.now }
								});

							    Notification.create(newNotification, function(err, notification) {
							        if (err) {
							        	console.log(err);
							        	callback(err, null);
							        }
					    			email.sendApprovedEmail(orderid, finalorders[0], callback);
							    });
					    		break;
					    	case 3:
								var newNotification = new Notification({
									_creator: finalorders[0]._adate._creator._id,
									subject: "Order Purchase",
									content: "A new order has been purchased.",
									_order: orderid,
									type: "purchase",
									date: { type: Date, default: Date.now }
								});

							    Notification.create(newNotification, function(err, notification) {
							        if (err) {
							        	console.log(err);
							        	callback(err, null);
							        }
					    			email.sendPaymentEmail(orderid, finalorders[0], callback);
							    });
					    		break;
					    	default:
					    		callback(err, finalorders);
					    		break;
					    }
						
					} else {
						console.log("populate error");
					    callback(err, null);
					}
				});
			} catch (e) {
				console.log("We've got an order with null adate!");				
			}
		} else{
		    callback(err, null);
		}
	});    
};

