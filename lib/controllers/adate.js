'use strict';

var mongoose = require('mongoose'),
    Adate = mongoose.model('Adate'),
    Order = mongoose.model('Order'),
    ArchiveAdate = mongoose.model('ArchiveAdate'),
    order = require('./order');

function AdateArchive(id, res, callback) {
	// Archive Order
	Adate.findById(id, function (err, doc) {
		ArchiveAdate.create(doc, function(err, ArchiveOrder) {
	        if (err) {
	        	console.log(err);
	        	return res.send(500);
	        }

	        // Find and archive all orders for this Adate
	        Order.find( { _adate: id }, function (err, items) {
				if (err) {
			       	console.log(err);
	        		return res.send(500);
				}

				// loop through and archive
				for (var i = 0; i < items.length; i++) {
			        order.OrderArchive(items[i]._id, res, null);			        						
				}
	
				// Delete Adate			
				Adate.remove({_id: id}, function (err) {
			        if (err) {
			        	console.log(err);
	        			return res.send(500);
			        }
			        if (callback) callback();
			    });
			});
	    });
	});	
};

/**
 * Get awesome things
 */
exports.AllAdates = function(req, res) {
  return Adate.find(function (err, Adates) {
    if (!err) {
      return res.json(Adates);
    } else {
      return res.send(err);
    }
  });
};

exports.ComingAdates = function(req, res) {
  var today = new Date();
  var topthresh = new Date(today);
  topthresh.setDate(today.getDate()+31);
  return Adate.find({date: { $lt: topthresh } }).sort({date: 1}).populate('_person').populate('_creator').exec(function (err, Adates) {
    if (!err) {
        return res.json(Adates);
    } else {
        return res.send(err);
    }
    
  });
};

exports.PersonAdates = function(req, res) {
	if (req.user){
		var userid = req.user._id;
		var AdatePerson = req.params.person;		
		return Adate.find( { _person: AdatePerson }).sort("date").exec(function (err, Adates) {
			if (!err) {
				return res.json(Adates);
			} else{
				return res.send(err);
			}
		});
	} else {
		return null;
	}
};  

exports.AAdate = function(req, res) {
	if (req.user){
		var userid = req.user._id;
		var AdateId = req.params.id;
		return Adate.findOne( { _creator: userid, _id: AdateId } ,function (err, Adate) {
			if (!err) {
				return res.json(Adate); 
			} else{
				return res.send(err);
			}
		});
	} else {
		return null;
	}
};

exports.AdateSave = function(req, res) {
	if (req.body){
	    Adate.findByIdAndUpdate(req.params.id, req.body, function (err, doc) {
	        if (err) {
	        	console.log(err);
	        	return res.send(400);
	        }
			return res.json(doc); 
	        //res.send(200);
	    });
	} else {
		return res.status(400).send('There was no adate data in the request to update. Please try again!');
	}
};

exports.AdateUpdateStatusPaid = function(id, status) {
	Adate.findById(id, function (err, doc) {
        if (err) {
        	console.log(err);
        }
		var newdate = new Date(parseInt(doc.date.getFullYear()) + 1,doc.date.getMonth(),doc.date.getDate());
		var adatebody = { date: newdate, status: status };
	    Adate.findByIdAndUpdate(id, adatebody, function (err, doc) {
	        if (err) {
	        	console.log(err);
	        }
	    });
	});	
};

exports.AdateUpdateStatus = function(id, status) {
	var adatebody = { status: status };
    Adate.findByIdAndUpdate(id, adatebody, function (err, doc) {
        if (err) {
        	console.log(err);
        }
    });
};

exports.AdateCreate = function(req, res) {
	if (req.body){
		var newAdate = new Adate(req.body);
		newAdate._creator = req.user._id;
	    Adate.create(newAdate, function(err, Adate) {
	        if (err) {
	        	console.log(err);
	        	return res.send(400);
	        }
	        res.status(200).send(Adate);
	    });
	} else {
		return res.status(400).send('There was no adate data in the request to create. Please try again!');
	}
};

exports.AdateDelete = function(req, res) {
    /*Adate.remove({_id: req.params.id}, function(err) {
        if (err) {
        	console.log(err);
        	return res.send(400);
        }
        res.send(200);
    });*/
	AdateArchive(req.params.id, res, function(){
		return res.send(200);		
	})
};

exports.AdateArchive = AdateArchive;
