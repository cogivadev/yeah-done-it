'use strict';

var mongoose = require('mongoose'),
    Order = mongoose.model('Order'),
    Payment = mongoose.model('Payment'),
    Orders = require('./order'),
    notification = require('./notification'),
	//stripe = require("stripe")("sk_test_B7XQBtDJhEOCadOFTf1fXTwT"); // TEST
	stripe = require("stripe")("sk_live_UzOwqwWVImpINumBxiOQsLKj"); // LIVE

exports.chargeCard = function(req, res) {
	if (req.user){
		var orderid = req.params.id;
		Order.findById(orderid, function (err, order) {
			if (err) {
				console.log("error", err);
    			return res.send(400);
			} else{
				// Send payment to Stripe
				// (Assuming you're using express - expressjs.com)
				// Get the credit card details submitted by the form
				var stripeToken = req.body.id;

				var charge = stripe.charges.create({
				  amount: order.details.listPrice.Amount, // amount in cents, again
				  currency: "gbp",
				  source: stripeToken,
				  description: order.details.title
				}, function(err, charge) {
					if (err && err.type === 'StripeCardError') {
					    // The card has been declined
	    				return res.send("declined");
					} else if (err) {
					  	return res.send(400);
					}
					console.log(charge);
					// Update OrderStatus (notifies and sends email)
					Orders.ApproveOrder(charge, orderid, function(neworder){
						var thispayment = { 
							_order: orderid,
							_stripe: JSON.stringify(charge)
						}
					    Payment.create(thispayment, function(err, paym) {
					        if (err) {
					        	console.log(err);
				        		return res.status(200).send(neworder);
					        }
				        	return res.status(200).send(neworder);
					    });						
					});
					// Add payment data to payment
				});
			}
		});

	} else {
    	return res.send(400);
	}
};
