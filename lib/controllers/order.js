'use strict';

var mongoose = require('mongoose'),
    Order = mongoose.model('Order'),
    ArchiveOrder = mongoose.model('ArchiveOrder'),
    notification = require('./notification'),
    adate = require('./adate');

var pidGenerator = function() {
	var result = '';
	var chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	var length = 4;
    for (var i = length; i > 0; --i) result += chars[Math.round(Math.random() * (chars.length - 1))];
    return result;
};


function OrderArchive(id, res, callback) {
	// Archive Order
	Order.findById(id, function (err, doc) {
		ArchiveOrder.create(doc, function(err, ArchiveOrder) {
	        if (err) {
	        	console.log(err);
	        }
			// Delete Order			
			Order.remove({_id: id}, function (err) {
		        if (err) {
		        	console.log(err);
		        }
		    });
		    if (callback) callback();
	    });
	});	
};


exports.getPersonOrders = function(req, res) {
	if (req.user){
		var orderPerson = req.params.person;		
		return Order.find( { _person: orderPerson }).populate('_adate').exec(function (err, orders) {
			if (!err) {
				return res.json(orders);
			} else{
				return res.send(err);
			}
		});
	} else {
		return null;
	}
};

exports.getUserOrders = function(req, res) {
	if (req.user){
		var userid = req.user._id;	
		return Order.find( { _user: userid }).populate('_adate').populate("_person").exec(function (err, orders) {
			if (!err) {
				return res.json(orders);
			} else{
				return res.send(err);
			}
		});
	} else {
		return null;
	}
};

exports.OrderCreate = function(req, res) {
	if (req.body){
		var newOrder = new Order(req.body);
	    Order.create(newOrder, function(err, Order) {
	        if (err) {
	        	console.log(err);
	        	return res.send(400);
	        }
	        // UPDATE RELEVANT ADATE STATUS
	        var adstatus = 0;
	        switch(Order.status) {
	        	case 1:
	        	case 2:
	        		adstatus = Order.status;
	        		break;
	        	default:
	        		adstatus = 3; 
	        		break;
	        }
	        adate.AdateUpdateStatus(Order._adate, adstatus);

	        // notification based on order status
    		notification.NotificationCreate(req.body.status, Order._id, function(err, info) {
    			if (err) {
	        		return res.send(400);    				
    			}
	        	res.status(200).send(info);
    		});
	    });
	} else {
		return res.status(400).send('There was no order data in the request to create. Please try again!');
	}
};

exports.OrderRemind = function(req, res) {
	notification.NotificationRemind(req.params.id, function(err, info) {
		if (err) {
    		return res.send(400);    				
		}
    	res.status(200).send(info);
	});	        	
};

exports.ApproveOrder = function(payment, orderid, callback) {
	// Set up the call body
	var thedate = new Date();
	var theMonth = thedate.getMonth() + 1;
	if (theMonth < 10) theMonth = "0" + theMonth;
	var newPID = pidGenerator() + "_" + thedate.getFullYear() + theMonth + thedate.getDate() + thedate.getHours() + thedate.getMinutes() + thedate.getMilliseconds(); 
	var approveData = {
		payment: {
			date: thedate,
			pid: newPID,
			card: "xxxx xxxx xxxx " + payment.source.last4
		}, 
		status: 2
	};
	Order.findByIdAndUpdate(orderid, approveData, function (err, doc) {
        if (err) {
        	console.log(err);
        	return res.send(400);
        }
        // UPDATE RELEVANT ADATE STATUS
        adate.AdateUpdateStatus(doc._adate, 2);
		notification.NotificationCreate(2, orderid, function(err, info) { });
		if (callback) callback(doc);
    });	
};

var PaidForOrder = function(payment, orderid, callback) {
	// Set up the call body
	Order.findById(orderid, function (err, doc) {
		var orderData = {
			datesent: new Date(),
			status: 3
		};
		Order.findByIdAndUpdate(orderid, orderData, function (err, doc) {
	        if (err) {
	        	console.log(err);
				if (callback) callback(err, doc);
	        }
	        // UPDATE RELEVANT ADATE STATUS
	        adate.AdateUpdateStatusPaid(doc._adate, 0);
			notification.NotificationCreate(3, orderid, function(err, info) { });
			if (callback) callback(err, doc);
	    });	
	});	
};

exports.UpdateStatus = function(req, res) {	
	if (req.body){
		if (req.body.status == 3)
		{
			PaidForOrder(null, req.body._id, function (err, doc){
				if (err) {
		        	console.log(err);
		        	return res.send(400);
		        } 
		        return res.send(200);
			});
		} else {
			Order.findByIdAndUpdate(req.params.id, req.body, function (err, doc) {
		        if (err) {
		        	console.log(err);
		        	return res.send(400);
		        }
		        // UPDATE RELEVANT ADATE STATUS
		        var adstatus = 0;
		        switch(doc.status) {
		        	case 1:
		        	case 2:
		        		adstatus = doc.status;
		        		break;
		        	default:
		        		adstatus = 0; 
		        		break;
		        }
		        adate.AdateUpdateStatus(doc._adate, adstatus);
				notification.NotificationCreate(req.body.status, req.params.id, function(err, info) {
	    			if (err) {
		        		return res.send(400);    				
	    			}
		        	res.status(200).send(info);
	    		});	        
		    });			
		} 
	} else {
		return res.status(400).send('There was no order status data in the request to update. Please try again!');
	}
};

exports.OrderDelete = function(req, res) {
	/*Order.remove({_id: req.params.id}, function (err) {
        if (err) {
        	console.log(err);
        	return res.send(400);
        }
        res.send(200);
    });*/
	OrderArchive(req.params.id, res, function(){
		return res.send(200);		
	})
};

exports.OrderArchive = OrderArchive;