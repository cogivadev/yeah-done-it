'use strict';

var mongoose = require('mongoose'),
    Person = mongoose.model('Person'),
    ArchivePerson = mongoose.model('ArchivePerson'),
    Adate = mongoose.model('Adate'),
    adate = require('./adate');

/**
 * Get awesome things
 */
exports.AllPersons = function(req, res) {
  return Person.find(function (err, Persons) {
    if (!err) {
      return res.json(Persons);
    } else {
      return res.send(err);
    }
  });
};

exports.MyPersons = function(req, res) {
	if (req.user){
		var userid = req.user._id;
		return Person.find( { _creator: userid }).sort("name").exec(function (err, Persons) {
			if (!err) {			
				return res.json(Persons);
			} else{
				return res.send(err);
			}
		});
	} else {
		return null;
	}
};  

exports.APerson = function(req, res) {
	if (req.user){
		var userid = req.user._id;
		var PersonId = req.params.id;
		return Person.findOne( { _creator: userid, _id: PersonId } ,function (err, Person) {
			if (!err) {
				return res.json(Person); 
			} else{
				return res.send(err);
			}
		});
	} else {
		return null;
	}
};

exports.PersonSave = function(req, res) {
	if (req.body){
	    Person.findByIdAndUpdate(req.params.id, req.body, function (err, doc) {
	        if (err) {
	        	console.log(err);
	        	return res.send(400);
	        }
			return res.json(doc); 
	        //res.send(200);
	    });
	} else {
		return res.status(400).send('There was no person data in the request to update. Please try again!');
	}
};

exports.PersonCreate = function(req, res) {
	if (req.body){
		var newPerson = new Person(req.body);
		newPerson._creator = req.user._id;
	    Person.create(newPerson, function(err, Person) {
	        if (err) {
	        	console.log(err);
	        	return res.send(400);
	        }
	        res.status(200).send(Person);
	    });
	} else {
		return res.status(400).send('There was no person data in the request to create. Please try again!');
	}
};

exports.PersonDelete = function(req, res) {
	// Remove all dates too
	Person.findById(req.params.id, function (err, doc) {
		ArchivePerson.create(doc, function(err, ArchivePerson) {
	        if (err) {
	        	console.log(err);
	        	return res.send(400);
	        }
	        // Find and archive all orders for this Adate
	        Adate.find( { _person: doc._id }, function (err, adates) {
				if (err) {
					console.log(err);
	        		return res.send(500);
				} 
				// loop through and archive
				for (var i = 0; i < adates.length; i++) {
					console.log(adates[i]._id);
			        adate.AdateArchive(adates[i]._id, res, null);		        						
				}
	
				// Delete Adate			
				Person.remove({_id: req.params.id}, function (err) {
			        if (err) {
			        	console.log(err);
			        	return res.send(500);
			        }
			        return res.send(200);
			    });
			});
	    });
	});	    
};