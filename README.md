# Yeah Done It

An app to help time strapped men to ensure they never forget a birthday or important date again!  
Built using Cogiva MEAN Stack Boilerplate

Node (with Express) provides the RESTful API.
Angular provides the frontend and accesses the API. 
MongoDB provides storage.

## Development Update Notes

### November 2014 
- Initial MVP build for testing model
- First release of web and android app

###-----------------------------------

## Requirements

- [Node and npm](http://nodejs.org)
- [MongoDB](http://www.mongodb.org)
- [Bower](http://bower.io/)
- Knowledge of NodeJS, AngularJS & MongoDB

## Installation

1. Clone the repository: `git clone git@bitbucket.org:Cogiva/cogiva-basic-boilerplate.git`
2. Install the application: `npm install`
3. Install the FE Dependancies: `bower install`
4. Start the server: `grunt serve`
5. View in browser at `http://localhost:9000` < Will Auto load from the grunt server command!

## Further Info

Build by: Ben Drury ( @Cogiva / www.cogiva.com / ben@cogiva.com )
