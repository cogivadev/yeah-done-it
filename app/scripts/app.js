'use strict';

angular.module('yeahdoneit', [
  'ngCookies',
  'ngResource',
  'ngSanitize',
  'ngRoute',
  'xeditable',
  'ui.bootstrap',
  'angularModalService',
  'angularPayments',
  'angular-intro',
  'ngTouch'
])
  .config(function ($routeProvider, $locationProvider, $httpProvider, datepickerConfig, datepickerPopupConfig) {
    //Stripe.setPublishableKey('pk_test_cMHNNNKHnoWvzaq3dzWWx18q'); // TEST
    Stripe.setPublishableKey('pk_live_JY9F56ZQxlwMOd9YNAGaa0ki'); // LIVE
    datepickerConfig.showWeeks = false;
    datepickerPopupConfig.showButtonBar = false;

    $routeProvider
      .when('/', {
        templateUrl: 'partials/main',
        controller: 'MainCtrl'
      })
      .when('/people', {
        templateUrl: 'partials/mypeople',
        controller: 'MypeopleCtrl',
        authenticate: true
      })
      .when('/login', {
        templateUrl: 'partials/login',
        controller: 'LoginCtrl'
      })
      .when('/signup', {
        templateUrl: 'partials/signup',
        controller: 'SignupCtrl'
        //templateUrl: 'partials/login',
        //controller: 'LoginCtrl'
      })
      .when('/amazon-store', {
        templateUrl: 'partials/amazon',
        controller: 'AmazonCtrl',
        authenticate: true
      })
      .when('/settings', {
        templateUrl: 'partials/settings',
        controller: 'SettingsCtrl',
        authenticate: true
      })
      .when('/terms', {
        templateUrl: 'partials/terms',
        controller: 'PlainCtrl'
      })
      .when('/privacy', {
        templateUrl: 'partials/privacy',
        controller: 'PlainCtrl'
      })
      .when('/contact', {
        templateUrl: 'partials/contact',
        controller: 'ContactCtrl'
      })
      .when('/coming-dates', {
        templateUrl: 'partials/coming-dates',
        controller: 'ComingDatesCtrl',
        authenticate: true
      })
      .when('/notifications', {
        templateUrl: 'partials/notifications',
        controller: 'NotificationsCtrl',
        authenticate: true
      })
      .when('/orders', {
        templateUrl: 'partials/orders',
        controller: 'OrdersCtrl',
        authenticate: true
      })
      .when('/admin-users', {
        templateUrl: 'partials/admin-users',
        controller: 'AdminUsersCtrl',
        authenticate: true
      })
      .when('/bug', {
        templateUrl: 'partials/bug',
        controller: 'BugCtrl',
        authenticate: true
      })
      .when('/four-oh-four', {
        templateUrl: 'partials/four-oh-four',
        controller: 'FourOhCtrl'
      })
      .when('/register-confirm', {
        templateUrl: 'partials/confirm',
        controller: 'ConfirmCtrl'
      })
      .when('/credits', {
        templateUrl: 'partials/credits',
        controller: 'Credits'
      })
      .otherwise({
        redirectTo: '/four-oh-four'
      });
      
    $locationProvider.html5Mode(true);
      
    // Intercept 401s and redirect you to login
    $httpProvider.interceptors.push(['$q', '$location', function($q, $location) {
      return {
        'responseError': function(response) {
          if(response.status === 401) {
            $location.path('/login');
            return $q.reject(response);
          }
          else {
            return $q.reject(response);
          }
        }
      };
    }]);
  })
  .run(function ($rootScope, $location, Auth, editableOptions, editableThemes) {

    editableThemes.bs3.inputClass = 'input-sm';
    editableThemes.bs3.inputClass = 'btn-sm';
    editableOptions.theme = 'bs3';

    // Redirect to login if route requires auth and you're not logged in
    $rootScope.$on('$routeChangeStart', function (event, next) {
      
      if (next.authenticate && !Auth.isLoggedIn()) {
        $location.path('/login');
      }

      if (Auth.isadminroute($location.path()) && !Auth.userisadmin()) {
        $location.path('/four-oh-four');
      }
    });
  })  
  .filter('unescape', function() {
    return window.decodeURIComponent;
  })  
  .filter('escape', function() {
    return window.encodeURIComponent;
  });