'use strict';

angular.module('yeahdoneit')
  .controller('SettingsCtrl', function ($scope, User, Auth, Styling, Alerts) {
    $scope.errors = {};
    
    Styling.setBG();
    Styling.setTitle("Settings | Yeah Done It");

    $scope.changePassword = function(form) {
      $scope.submitted = true;

      if(form.$valid) {
        Auth.changePassword( $scope.user.oldPassword, $scope.user.newPassword )
        .then( function() {
          //$scope.user.oldPassword = null;
          //$scope.user.newPassword = null;
           Alerts.setMessage(
                Alerts.getAlertTypes().alertType_Success, 
                Alerts.getAlertTitles().alertTitle_Success, 
                Alerts.getAlertMessages().alertMessage_ChangePasswordSuccess, 
                true);
        })
        .catch( function() {
          form.password.$setValidity('mongoose', false);
          //$scope.errors.other = 'Incorrect password';
           Alerts.setMessage(
                Alerts.getAlertTypes().alertType_Error, 
                Alerts.getAlertTitles().alertTitle_Error, 
                Alerts.getAlertMessages().alertMessage_ChangePasswordIncorrect, 
                false);
        });
      }
		};
  });
