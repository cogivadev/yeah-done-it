'use strict';

angular.module('yeahdoneit')
  .controller('BugCtrl', function ($scope, $http, Styling, $rootScope, Alerts) {

  	Styling.setBG();
    Styling.setTitle("Bug Report | Uh-Oh! Not Done It!");
    console.log("RSCU", $rootScope.currentUser);
    $scope.sending = null;

    $scope.sendBugReport = function() {
    	console.log("sending...");
    	console.log("Bug...", $scope.bug);
        $scope.sending=true;
        var self = this;

        var promise = $http.post('/api/bugreport/', $scope.bug);
        promise.then(function(response){
          console.log(response.data);
          Alerts.setMessage(
                Alerts.getAlertTypes().alertType_Success, 
                Alerts.getAlertTitles().alertTitle_Success, 
                Alerts.getAlertMessages().alertMessage_BugReportPosted, 
                false);
            $scope.resetbug();
            $scope.sending=false;
        }).catch(function(e){
           throw e;
        }).then(function(res){
            // do more stuff
        }).catch(function(e){
            // handle errors in processing or in error.
           Alerts.setMessage(
                Alerts.getAlertTypes().alertType_Error, 
                Alerts.getAlertTitles().alertTitle_Error, 
                Alerts.getAlertMessages().alertMessage_GlobalTryAgain, 
                false);
            $scope.sending=true;
        });  
    };

    $scope.resetbug = function(){
        $scope.bug =  {
            title: null,
            page: null, 
            content: null,
            email: $rootScope.currentUser.email
        };
    };

    if (!$scope.bug) {
        $scope.resetbug();
    }
    //$scope.resetbug();

  });
