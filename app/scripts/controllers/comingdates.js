'use strict';

angular.module('yeahdoneit')
  .controller('ComingDatesCtrl', function ($scope, Styling, $http, Person, Adate, Order, Alerts, $timeout, $filter, AmazonAPI, ModalService) {

    Styling.setBG(); 
    Styling.setTitle("ADMIN | Up Coming Dates");

	  $scope.activeItem = null;
    $scope.adateOS = 132; 
	  $scope.adateOSBase = 132; 
    $scope.Styling = Styling;   
    $scope.Adate = Adate;
    $scope.Order = Order;
    $scope.refreshing = false;
    $scope.showsearch = true;
    $scope.searchterm = "";
    $scope.AmazonAPI = AmazonAPI;
    $scope.AmazonLoading = false;

    Adate.getComingDates();  

    $scope.loadAdate = function($event, adateid) {
      Styling.setLoading(true);
    	$scope.activeItem = adateid;

    	//Scroll #person Element to correct place
    	$scope.adateOS = $scope.adateOSBase - $(window).scrollTop();
    	var topVal = $event.currentTarget.getBoundingClientRect().top - $scope.adateOS;

    	jQuery("#anAdate").animate({ top: topVal }, 500, function(){});
        
        if ($scope.Adate.TheAdate === null || adateid !== $scope.Adate.TheAdate._id)
        {
            Order.Orders = null;
            Adate.TheAdate = $filter('filter')(Adate.Adates, { _id: adateid })[0];
            // Load Orders for person     
            Order.getPersonOrders(Adate.TheAdate._person._id);
        }
        Styling.setLoading(false);        
    }; 

    $scope.clearAdate = function($event, searchshow) {
        $scope.showsearch = searchshow;
        console.log("doing it man");
        Adate.clearTheAdate();
        Order.clearOrders();
        jQuery("#anAdate").animate({ top: 0 }, 0, function(){});
        $scope.activeItem = null;
        $scope.searchterm = "";
        AmazonAPI.clearsearch(); 
    };

    $scope.refreshAdates = function() {
        $scope.clearAdate();
        Adate.clearAdate();
        Adate.getComingDates(); 
        $scope.activeItem = null;
        $scope.searchterm = "";
        AmazonAPI.clearsearch();   
    };

    $scope.amazonsearch = function(e) {
        // Test for Keypress
        e.preventDefault();
        self = this;
        var thebudget = parseInt($scope.Adate.TheAdate.budget) * 100;
        console.log("Budget", thebudget);
        if(e.which === 13) {
            // searchAmazon
            Styling.setAmazonLoading(true);
            console.log("searching...");
            AmazonAPI.search(self.searchterm, thebudget);                
        }
    };

    $scope.saveOrder = function(e, amazonindex, status) {
        e.preventDefault();
        
        // Create Order
        console.log(AmazonAPI.SearchItems[amazonindex].SmallImage[0].URL[0]);
        console.log(AmazonAPI.SearchItems[amazonindex].MediumImage[0].URL[0]);
        console.log(AmazonAPI.SearchItems[amazonindex].LargeImage[0].URL[0]);
        var am_details = {
          itemId: AmazonAPI.SearchItems[amazonindex].ASIN[0],
          pageUrl: AmazonAPI.SearchItems[amazonindex].DetailPageURL[0],
          tinyImage: AmazonAPI.SearchItems[amazonindex].SmallImage[0].URL[0].replace('http://','https://'),
          mediumImage: AmazonAPI.SearchItems[amazonindex].MediumImage[0].URL[0].replace('http://','https://'),
          largeImage: AmazonAPI.SearchItems[amazonindex].LargeImage[0].URL[0].replace('http://','https://'),
          title: AmazonAPI.SearchItems[amazonindex].ItemAttributes[0].Title[0],
          productGroup: AmazonAPI.SearchItems[amazonindex].ItemAttributes[0].ProductGroup[0]
        };

        console.log(am_details.tinyImage);
        console.log(am_details.mediumImage);
        console.log(am_details.largeImage);

        // Code for missing images
        if (AmazonAPI.SearchItems[amazonindex].SmallImage) {
          am_details.tinyImage = AmazonAPI.SearchItems[amazonindex].SmallImage[0].URL[0].replace('http://','https://');

        }
        if (AmazonAPI.SearchItems[amazonindex].MediumImage) {
          am_details.mediumImage = AmazonAPI.SearchItems[amazonindex].MediumImage[0].URL[0].replace('http://','https://');
        }            
        if (AmazonAPI.SearchItems[amazonindex].LargeImage) {            
          am_details.largeImage = AmazonAPI.SearchItems[amazonindex].LargeImage[0].URL[0].replace('http://','https://');
        }

        console.log(am_details.tinyImage);
        console.log(am_details.mediumImage);
        console.log(am_details.largeImage);
        
        // code for missing ListPrice
        //if (AmazonAPI.SearchItems[amazonindex].ItemAttributes[0].ListPrice) {
        if (AmazonAPI.SearchItems[amazonindex].Offers[0].Offer[0].OfferListing[0].Price) {
            var listPrice = {
              Amount: AmazonAPI.SearchItems[amazonindex].Offers[0].Offer[0].OfferListing[0].Price[0].Amount[0],
              CurrencyCode: AmazonAPI.SearchItems[amazonindex].Offers[0].Offer[0].OfferListing[0].Price[0].CurrencyCode[0],
              FormattedPrice: AmazonAPI.SearchItems[amazonindex].Offers[0].Offer[0].OfferListing[0].Price[0].FormattedPrice[0]
            };
            am_details.listPrice = listPrice;
            $scope.processSave(am_details, status);
        } else {
            var amzlink = AmazonAPI.SearchItems[amazonindex].DetailPageURL[0];
            ModalService.showModal({
              templateUrl: "../views/apaprice.html",
              controller: "apaprice",
              inputs: {
                link: amzlink,
              }
            }).then(function(modal) {
              // The modal object has the element built, if this is a bootstrap modal
              // you can call 'modal' to show it, if it's a custom modal just show or hide
              // it as you need to.
              modal.element.modal();
              modal.close.then(function(result) {
                if (result) {
                    var listPrice = {
                      Amount: result.replace("£","").replace(".",""),
                      CurrencyCode: "GBP",
                      FormattedPrice: result
                    };
                    console.log(listPrice);
                    am_details.listPrice = listPrice;
                    $scope.processSave(am_details, status);
                }
              });
            });            

        }
    };

    $scope.processSave = function(am_details, status) {
        console.log("saving");
        var newOrder = {
          _person: Adate.TheAdate._person._id,
          _user: Adate.TheAdate._creator._id,
          _adate: Adate.TheAdate._id,
          datedate: new Date(Adate.TheAdate.date),
          datesent: new Date('2014', '11', '17'),
          price: am_details.listPrice.FormattedPrice,
          status: status,
          details: am_details
        };

        Order.createPersonOrder(newOrder);  

        switch (status) {
            case 1:
            case 2:
            case 0:
                Adate.updateStatus(status, function() {
                    Adate.getFilteredLists();                    
                });               
                break;
            case 3:
                Adate.updateStatus(0, function() {
                    Adate.getFilteredLists();                    
                });
                break;
            default:
                break;
        }
    };

    $scope.deleteOrder = function(e, orderindex, id) {
       e.preventDefault();
       Order.deleteOrder(orderindex, id);
    };

    $scope.remindOrder = function(e, id) {
       e.preventDefault();
       Order.remindSuggest(id);
    };

    $scope.buyOrder = function(e, id) {
    };

    $scope.updateOrderStatus = function(e, orderid, status) {
        e.preventDefault();
        Order.updateOrderStatus(status, orderid, function() {
            Order.getFilteredLists();
            Adate.getFilteredLists();                    
        });
    };

    $scope.showPriceModal = function(amzlink) {
        // Just provide a template url, a controller and call 'showModal'.
        amzlink = "http://www.cogiva.com";
        ModalService.showModal({
          templateUrl: "../views/apaprice.html",
          controller: "apaprice",
          inputs: {
            link: amzlink,
          }
        }).then(function(modal) {
          // The modal object has the element built, if this is a bootstrap modal
          // you can call 'modal' to show it, if it's a custom modal just show or hide
          // it as you need to.
          modal.element.modal();
          modal.close.then(function(result) {
            if (result) {

            } else {

            }
            $scope.message = result ? "You said Yes" : "You said No";
          });
        });
    };

  });
