'use strict';

angular.module('yeahdoneit')
  .controller('AdminUsersCtrl', function ($scope, Styling, AdminUsers) {

    Styling.setBG();
    Styling.setTitle("ADMIN | Users");
    $scope.AdminUsers = AdminUsers;
    $scope.useremail = null;

    AdminUsers.getUsers();

    $scope.createInvite = function() {
    	$scope.showinvite = !$scope.showinvite;
    };

    $scope.sendInvite = function() {
    	$scope.sending = true;
    	console.log("AUC: sending..");
    	AdminUsers.sendInvite($scope.useremail, function(){
    		$scope.sending = false;
    	});
    };

  });
