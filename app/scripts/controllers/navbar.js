'use strict';

angular.module('yeahdoneit')
  .controller('NavbarCtrl', function ($scope, $location, Auth, $rootScope, Notifications, Styling) {

    $scope.showMenu = false;
    $scope.showAdminMenu = Auth.userisadmin();
    $scope.showAdminSubmenu = false;
    $scope.Styling = Styling;
    $scope.Notifications = Notifications;
    $scope.Notifications.getUnreadCount();

    $scope.$watch(function() {
      return $rootScope.currentUser;
    }, function() {
      $scope.showAdminMenu = Auth.userisadmin();
    }, true);

    $scope.$watch(function() {
      return $scope.showAdminSubmenu;
    }, function() {
    }, true);

    $scope.menu = [{
      'title': 'Home',
      'link': '/',
      'hide': 'currentUser',
      'faicon': 'fa-home'
    }, {
      'title': 'People',
      'link': '/people',
      'hide': '!currentUser',
      'faicon': 'fa-gift'    
    }, {
      'title': 'Orders',
      'link': '/orders',
      'hide': '!currentUser',
      'faicon': 'fa-gbp'    
    }, {
      'title': 'Notifications',
      'link': '/notifications',
      'hide': '!currentUser',
      'faicon': 'fa-exclamation-circle'
    }, {
      'title': 'Settings',
      'link': '/settings',
      'hide': '!currentUser',
      'faicon': 'fa-cog'
    }, {
      'title': 'Report Bug',
      'link': '/bug',
      'hide': '!currentUser',
      'faicon': 'fa-bug'
    }];

    $scope.adminMenu = [{
      'title': 'Admin', 
      'link': '',
      'faicon': 'fa-flask'
    }];

    $scope.adminSubmenu = [{
        'title': 'Coming Dates',
        'link': '/coming-dates',
        'faicon': 'fa-calendar'
    },{
        'title': 'User Admin',
        'link': '/admin-users',
        'faicon': 'fa-users'
    },{
        'title': 'Amazon Store',
        'link': 'http://astore.amazon.co.uk/yedoit0e-21',
        'faicon': 'fa-shopping-cart'
    }];
    

    $scope.logout = function() {
      Auth.logout()
      .then(function() {
        $location.path('/');
      });
    };
    
    $scope.isActive = function(route) {
      return route === $location.path();
    };

    $scope.toggleMenus = function (e) {
      if (e) { e.preventDefault(); }
      //$scope.showMenu = !$scope.showMenu;
      Styling.showMenu = !Styling.showMenu;
    };

    $scope.changeSubMenuShow = function(setVal){
      setVal = typeof setVal !== 'undefined' ? setVal : null;
      if (setVal)
      {
        $scope.showAdminSubmenu = setVal;
      }
      else
      {
        $scope.showAdminSubmenu = !$scope.showAdminSubmenu;
      }
    };
  });
