'use strict';

angular.module('yeahdoneit')
	.controller('apaprice', function($scope, $sce, link, close) {
 
 	 $scope.thelink = $sce.trustAsResourceUrl(link);
 	 $scope.apaenteredprice = "";

	 $scope.close = function(result) {
	 	if (result) {
	 		close($scope.apaenteredprice, 500); // close, but give 500ms for bootstrap to animate	 		
	 	} else {
	 		close(false, 500); // close, but give 500ms for bootstrap to animate	 			 		
	 	}
	 };
 });