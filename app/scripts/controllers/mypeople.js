'use strict';

angular.module('yeahdoneit')
  .controller('MypeopleCtrl', function ($scope, $http, Styling, Person, Adate, Alerts, $timeout, $filter, Order, $location) {

    Styling.setBG();
    Styling.setTitle("People | Yeah Done It!");

    $scope.Styling = Styling;
	$scope.activeItem = null;
    $scope.personOS = 125;
	$scope.personOSBase = 125; 
    $scope.personDates = null;
    $scope.Person = Person;
    $scope.Order = Order;
    $scope.Adate = Adate;
    $scope.picker = { opened: false };
    $scope.tutorialon = false;
    $scope.editing = false;
    $scope.editeddate = null;
    $scope.editeddateitem = null;
    $scope.editingname = false; 
    $scope.editingaddress = false;
    $scope.editingpreferences = false;
    $scope.editinggenderage = false;

    $scope.genders = [
        {value: "Male", text: "Male" },
        {value: "Female", text: "Female"},
        {value: "Dunno", text: "Dunno"}
    ]

    $scope.ages = [
        {value: "0-5", text: "0-5" },
        {value: "6-10", text: "6-10"},
        {value: "11-15", text: "11-15"},
        {value: "16-20", text: "16-20"},
        {value: "21-25", text: "21-25"},
        {value: "26-30", text: "26-30"},
        {value: "31-40", text: "31-40"},
        {value: "41-50", text: "41-50"},
        {value: "51-60", text: "51-60"},
        {value: "61-70", text: "61-70"},
        {value: "71-79", text: "71-79"},
        {value: "80+", text: "80+"}
    ]

    Adate.clearAdate();
    Person.getMyPersons();

    $scope.showGender = function() {
        if ($scope.Person.ThePerson) {
            var selected = $filter('filter')($scope.genders, {value: $scope.Person.ThePerson.gender});
            return ($scope.Person.ThePerson.gender && selected.length) ? selected[0].text : 'Not set';            
        } else {
            return 'Not Set';
        }
    };

    $scope.showAge = function() {
        if ($scope.Person.ThePerson) {
            var selected = $filter('filter')($scope.ages, {value: $scope.Person.ThePerson.age});
            return ($scope.Person.ThePerson.age && selected.length) ? selected[0].text : 'Not set';            
        } else {
            return 'Not Set';
        }
    };

    $scope.loadPerson = function($event, personid) {

        Styling.setLoading(true);
    	$scope.activeItem = personid;

    	//Scroll #person Element to correct place
    	$scope.personOS = $scope.personOSBase - $(window).scrollTop();
    	var topVal = $event.currentTarget.getBoundingClientRect().top - $scope.personOS;
        if (topVal < 0) { topVal = 7; }

        Adate.getPersonAdates(personid, function() {
            if (Person.ThePerson === null || personid !== Person.ThePerson._id)
            {
                $scope.removeAllDeleteConfirms();
                Person.ThePerson = $filter('filter')($scope.Person.Persons, {_id:personid})[0];
                Order.Orders = null;
                Order.getPersonOrders(Person.ThePerson._id);
            }
            if ($scope.tutorialon) {
                $('.introjs-nextbutton').click();
            }
        });

    	jQuery("#person").animate({ top: topVal }, 500, function(){});        
    };

/*<span editable-text="item.description" e-form="editableDocumentDescription" buttons="no" ng-click="methodToExecute(editableDocumentDescription)">{{item.description}}</span>

and in your code, you can execute an action:

function methodToExecute(object) {
   object.$show(); // for example
}*/



    $scope.clearPerson = function(e){
        e.stopPropagation();
        e.preventDefault();
        Person.clearPerson();
        Adate.clearAdate();        
        $scope.activeItem = null;
    };

    $scope.openPicker = function() {
        $timeout(function() {
          $scope.picker.opened = true;
        });
    };
      
    $scope.closePicker = function() {

        $scope.picker.opened = false;
    };

    $scope.showedit = function(theInput, theitem) {
        switch (theInput) {
            case 'name':
                // add class to overlay and add class to editing field
                $scope.editing = true; 
                $scope.editingname = true;
                break;
            case 'likes':
                // add class to overlay and add class to editing field
                $scope.editing = true;
                $scope.editingpreferences = true;
                break;
            case 'address':
                // add class to overlay and add class to editing field
                $scope.editing = true;
                $scope.editingaddress = true;
                break;
            case 'genderage':
                // add class to overlay and add class to editing field
                $scope.editing = true;
                $scope.editinggenderage = true;
                break;
            case 'done':
                $scope.editing = false;
                $scope.editeddate = null;
                $scope.editeddateitem = null;                
                $scope.editingname = false;
                $scope.editingaddress = false;
                $scope.editingpreferences = false;  
                $scope.editinggenderage = false;              
                break;
            default:
                // Add showhighrel to id'd class
                $scope.editing = true;
                $scope.editeddate = theInput;
                $scope.editeddateitem = theitem;                
                break;
        }
    };

    $scope.beforePersonSave = function() {

        $scope.showedit('done');
    };

    $scope.afterPersonSave = function() {

        $scope.savePerson(); 
    };    

    $scope.beforeDateSave = function(dateindex) {

        $scope.showedit('done'); 
    };

    $scope.afterDateSave = function(dateindex) {
        $scope.saveAdate(dateindex, function() {
            if ($scope.tutorialon) {
                $('.introjs-nextbutton').click();
            }
        }); 
    };    

    $scope.savePerson = function() {
        Person.savePerson(Person.ThePerson, Person.ThePerson._id, function(){
            if ($scope.tutorialon) {
                $('.introjs-nextbutton').click();
            }
        });
    };

    $scope.createPerson = function(e) {
        var newPerson = {
          name: "A Friend",
          address: "Pooh Bear's House, 100 Aker Wood.",
          preferences: "Cheese, wine, top trumps, libraries, collecting pebbles.",
          gender: "Female",
          age: "20-25"
        };
        Person.createPerson(newPerson, function(person){                
            if (person) {
                $scope.loadPerson(e, person._id);    
                if ($scope.tutorialon) {
                    $('.introjs-nextbutton').click();
                }                            
            }
        });
    };

    $scope.updatePersonName = function(NewName){
        $scope.savePerson();
    };
    
    $scope.addDate = function () {
        Styling.setLoading(true);
        // Add Date
        var dt = new Date();
        var newAdate = {
            _person: Person.ThePerson._id,
            name: "Just Today!",
            date: dt,
            budget: 1,
            status: 0,
            message: "Well done Orville! What's next? Pigs?"
        };

        Adate.createAdate(newAdate, function() {
            if ($scope.tutorialon){
                $('.introjs-nextbutton').click();
            }
        });
    };

    $scope.saveAdate = function(dateindex, callback) {
        Styling.setLoading(true);        
        if (callback) {
            Adate.saveAdate($scope.Adate.Adates[dateindex], $scope.Adate.Adates[dateindex]._id, callback);
        } else {
            Adate.saveAdate($scope.Adate.Adates[dateindex], $scope.Adate.Adates[dateindex]._id);
        }
    };

    $scope.confirmDeleteDate = function (dateIndex) {
        $scope.Adate.Adates[dateIndex].confirmDelete = true;
    };

    $scope.updateOrderStatus = function(e, orderid, status) {
        switch (status) {
            case 0:
                $location.path ("/orders").search('order', orderid).search('todo', 'reject');
                break;
            case 2:
                $location.path ("/orders").search('order', orderid).search('todo', 'accept');
                break;
            default:
                break;
        }
    };

    $scope.cancelDeleteDate = function (dateIndex) {
        // Set Date.del
        $scope.Adate.Adates[dateIndex].confirmDelete = false;
    };

    $scope.deleteDate = function (deleteId, dateIndex) {

        Adate.deleteAdate(deleteId, dateIndex);        
    };

    $scope.confirmDeletePerson = function(e, personIndex) {
        e.stopPropagation();
        e.preventDefault();
        $scope.Person.Persons[personIndex].confirmDelete = true; 
    };

    $scope.cancelDeletePerson = function(e, personIndex) {
        e.stopPropagation();
        e.preventDefault();
        $scope.Person.Persons[personIndex].confirmDelete = false;
    };

    $scope.deletePerson = function (e, personIndex){
        e.stopPropagation();
        e.preventDefault();
        $scope.Person.Persons[personIndex].confirmDelete = false;        
        Person.deletePerson(personIndex);        
        jQuery("#person").animate({ top: 0 }, 500, function(){});
        Adate.clearAdate();
    };

    $scope.removeAllDeleteConfirms = function(){
        for(var i=0;i<$scope.Person.Persons.length;++i) {
            $scope.Person.Persons[i].confirmDelete = false;
        }        
        if ($scope.Adate.Adates){
            for(var j=0;j<$scope.Adate.Adates.length;++j) {
                $scope.Adate.Adates[j].confirmDelete = false;
            }
        }
    };

    //
    // Tutorial Intro
    //

        $scope.startTutorial = function() {
            $scope.tutorialon = true;
            $scope.CallMe();
        };

        $scope.CompletedEvent = function (scope) {
            $scope.tutorialon = false;
        };

        $scope.ExitEvent = function (scope) {
            $scope.tutorialon = false;
        };

        $scope.ChangeEvent = function (targetElement, scope) {
            if (angular.element(targetElement).hasClass('step1')) {
                // Nothing
            } else if (angular.element(targetElement).hasClass('step2')) {
                // Nothing
            } else if (angular.element(targetElement).hasClass('step3')) {
                // Nothing
            } 
        };

        $scope.BeforeChangeEvent = function (targetElement, scope) {

            if (angular.element(targetElement).hasClass('step1')) {
                // Nothing
            } else if (angular.element(targetElement).hasClass('step2')) {
                // add a person
            } else if (angular.element(targetElement).hasClass('step3')) {
            } 
        };

        $scope.AfterChangeEvent = function (targetElement, scope) {
            if (angular.element(targetElement).hasClass('step1')) {
                // Nothing
            } else if (angular.element(targetElement).hasClass('step2')) {
                // Nothing
            } else if (angular.element(targetElement).hasClass('step3')) {
                // Nothing
            }         
        };

        $scope.IntroOptions = {
            steps:[
            {
                element: '.step1',
                intro: "1. To start, click <b>ADD PERSON</b>....",
                position: 'left'
            },
            {
                element: '.step2',
                intro: '2. Person details appear over here. <b>CLICK on the plus sign</b> (top right) to add an event.',
                position: 'left'
            },
            {
                element: '.step3',
                intro: '3. Here\s the new event. <b>CLICK on the bold text to edit</b>. Hit RETURN to save when you\'re done. (For the date only put the next date it will occur - i.e. 10 Nov 2015)',
                position: 'left'
            },
            {
                element: '.step4',
                intro: '4. Person address and preferences are down here. <b>CLICK on those</b> to change them.',
                position: 'left'
            },
            {
                element: '.step5',
                intro: '5. If you rollover an item the delete icon appears. Rollover a person and click on the delete icon.',
                position: 'right'
            },
            {
                element: '.step6',
                intro: 'And that\'s how it all works. Simple!<br /><br />You set up your people and the special dates.  We\'ll suggest appropriate gifts, get you to confirm and then send them to arrive on the right date.<br /><br />Never forget again!',
                position: 'top'
            }],
            showStepNumbers: false,
            exitOnOverlayClick: false,
            exitOnEsc:true,
            nextLabel: '<strong>NEXT!</strong>',
            prevLabel: '<span style="color:grey">Previous</span>',
            skipLabel: '<i class="fa fa-sign-out"></i>Exit Tutorial',
            doneLabel: 'Yay! Finished',
            keyboardNavigation: false
        };

        $scope.ShouldAutoStart = false;
    //
  });
