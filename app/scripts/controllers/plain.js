'use strict';

angular.module('yeahdoneit')
  .controller('PlainCtrl', function ($scope, $http, Styling, $rootScope, $location) {

  	Styling.setBG();
  	switch ($location.path) {
  		case "/contact":
    		Styling.setTitle("Contact Us | Yeah Done It");
  			break;
  		case "/terms":
    		Styling.setTitle("Terms and Conditions | Yeah Done It");
  			break;
  		case "/privacy":
    		Styling.setTitle("Privacy | Yeah Done It");
  			break;
  		default:
  			break;
  	} 

    $scope.Styling = Styling;
 	 
  });
