'use strict';

angular.module('yeahdoneit')
  .controller('LoginCtrl', function ($scope, Auth, $location, Styling, $rootScope) {
    $scope.user = {};
    $scope.errors = {};

    Styling.setBG();
    Styling.setTitle("Login | Yeah Done It!");

    $scope.loginfb = function() {
      console.log("posting FB...");
      Auth.loginfb(function(){
        console.log($rootScope.currentUser);
        $location.path('/people');        
      });      
    };

    $scope.login = function(form) {
      $scope.submitted = true;
      
      if(form.$valid) {
        Auth.login({
          email: $scope.user.email,
          password: $scope.user.password
        })
        .then( function() {
          // Logged in, redirect to home
          $location.path('/people');
        })
        .catch( function(err) {
          err = err.data;
          $scope.errors.other = err.message;
        });
      }
    };
  });