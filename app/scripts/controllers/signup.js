'use strict';

angular.module('yeahdoneit')
  .controller('SignupCtrl', function ($rootScope, $scope, Auth, $location, Styling, $http, Alerts) {
    $scope.user = {};
    $scope.errors = {};

    Styling.setBG();
    Styling.setTitle("Sign Up | Yeah Done It");

    $scope.loading = false;
    $scope.allowregister = true;
    //$scope.invite = $location.search().invite;
    //$scope.email = $location.search().email;

    if ($rootScope.currentUser) {
      $location.path('/people');
    }

    //if invite check else show
    /*if ($scope.invite && $scope.email){
      // check invite
      var self = this;
      var promise = $http.get('/api/checkinvite/' + $scope.invite);
      promise.then(function(response){
        console.log("R.SI",response);
        if (response) {
          $scope.loading = false;
          $scope.allowregister = true;          
        } else {
          $scope.loading = false;
          $scope.allowregister = false;          
        }
      }).catch(function(e){
         throw e;
      }).then(function(res){
          // do more stuff
      }).catch(function(e){
          // handle errors in processing or in error.
         $scope.loading = false;
         $scope.allowregister = false;         
      }); 

    } else {
      $scope.loading = false;
      $scope.allowregister = false;
    }*/


    $scope.register = function(form) {
      $scope.submitted = true;
  
      if(form.$valid) {
        Auth.createUser({
          name: $scope.user.name,
          email: $scope.user.email,
          password: $scope.user.password
        })
        .then( function() {
          // Account created, redirect to home
          $location.path('/people');
        })
        .catch( function(err) {
          err = err.data;
          $scope.errors = {};

          // Update validity of form fields that match the mongoose errors
          angular.forEach(err.errors, function(error, field) {
            form[field].$setValidity('mongoose', false);
            $scope.errors[field] = error.message;
          });
        });
      }
    };
  });