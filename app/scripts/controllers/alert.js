'use strict';

angular.module('yeahdoneit')
  .controller('AlertCtrl', function ($scope, Alerts) {
    
    $scope.Alerts = Alerts;
    
    $scope.closeAlert = function() {
      Alerts.unsetMessage();
    };
  });
