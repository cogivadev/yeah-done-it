'use strict';

angular.module('yeahdoneit')
  .controller('NotificationsCtrl', function ($scope, $http, Styling, Notifications, $location, $filter) {

  	Styling.setBG();
    Styling.setTitle("Notifications");

    $scope.Notifications = Notifications;

    Notifications.getMyNotifications();

    $scope.viewOrder = function(orderid, notificationid) {
    	console.log(orderid);
    	this.markRead(notificationid);
		$location.path("/orders").search('order', orderid);
    };

    $scope.allRead = function(){
    	var self = this;
    	console.log("All Read...");    	
    	var unread =  $filter('filter')($scope.Notifications.Notifications, { read: false });
    	console.log("length", unread.length);
    	for(var i=0; i < unread.length; i++) {
    		var NoteIndex = Notifications.Notifications.indexOf(unread[i]);
        	Notifications.Notifications[NoteIndex].read = true;
    		self.markRead(unread[i]._id);
    	}
    	console.log("All Read Done");
    };

    $scope.markRead = function(notificationid){
    	console.log("Marking Read", notificationid);
    	Notifications.decrement(function(){
    		console.log("Scope currentUnread", $scope.Notifications.currentUnread);
    	});
    	Notifications.markAsRead(notificationid, function(){});    	
    };    

  });
