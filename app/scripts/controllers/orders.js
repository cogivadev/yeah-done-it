'use strict';

angular.module('yeahdoneit')
  .controller('OrdersCtrl', function ($rootScope, $scope, $http, Styling, Notifications, $location, Order, Alerts, Payment) {

  	Styling.setBG();
    Styling.setTitle("Orders");

	  $scope.activeItem = null;
    $scope.personOS = 125; 
	  $scope.personOSBase = 125; 
    $scope.Order = Order;
    $scope.Todo = $location.search().todo;
    $scope.paymentProcessing = null;

    Order.getUserOrders(function(){
        if ($location.search().order) {
          console.log("ls", $location.search().order);
          Order.setActiveOrder($location.search().order);
          console.log("AO", Order.ActiveOrder);
          if ($location.search().todo && $location.search().todo == 'accept') {
            $scope.acceptOrder($location.search().order);
          } else if ($location.search().todo && $location.search().todo == 'reject') {
            $scope.rejectOrder($location.search().order);
          }
        } else {
          Order.setActiveOrder(null);
        }   
    }); 

    $scope.showAll = function(e) {
        console.log("showing all...")
        e.stopPropagation();
        e.preventDefault();
        $scope.Todo = null;
        Order.setActiveOrder(null);
    };
    
    $scope.acceptClick = function(orderid) {
      console.log("Accepting Order");
      $scope.Todo = "accept";
      Order.setActiveOrder(orderid);
    };

    $scope.rejectClick = function(orderid) {
      console.log("Rejecting Order");
      $scope.Todo = "reject";
      Order.setActiveOrder(orderid);
      $scope.rejectOrder(orderid);
    };

    $scope.acceptOrder = function(orderid) {
      if (Order.ActiveOrder.status == 1){
        // Pay for order and then 
        //$scope.updateOrderStatus(orderid, 2);        
      } else {
        console.log("Order.setActiveOrder",Order.ActiveOrder.status);
        $scope.Todo = null;
        Order.setActiveOrder(null);
        Alerts.setMessage(
            Alerts.getAlertTypes().alertType_Warning, 
            Alerts.getAlertTitles().alertTitle_Warning, 
            Alerts.getAlertMessages().alertMessage_WarningAlreadyAccepted,         
            false);
      }
    };

    $scope.cancelAccept = function (){
        $scope.Todo = null;
    };

    $scope.payProcess = function() {
      $scope.paymentProcessing = true;
    };

    $scope.stripeCallback = function (code, result) { 
        if (result.error) {
            console.log("it failed! error: ", result.error.message);
            Alerts.setMessage(
                Alerts.getAlertTypes().alertType_Warning, 
                Alerts.getAlertTitles().alertTitle_Warning, 
                result.error.message,
                false);
            $scope.paymentProcessing = false;
        } else {
            var orderid = Order.ActiveOrder._id;
            //send to server for charging, status update, add payment info
            Payment.chargeCard(orderid, result, function(neworder) {
              console.log("update Active Order", Order.ActiveOrder);
              Order.ActiveOrder.payment = neworder.payment;
              Order.ActiveOrder.status = neworder.status;
              console.log("updated", Order.ActiveOrder);
              $scope.Todo = null;
              $scope.paymentProcessing = false;              
            });
            //TODO
            console.log("success! token:", result.id);
            console.log("result:", result);
            console.log("card:", result.card);
        }
    };

    $scope.rejectOrder = function(orderid) {
      if (Order.ActiveOrder.status == 1){
        $scope.updateOrderStatus(orderid, 0);
      } else {
        console.log("Order.setActiveOrder",Order.ActiveOrder.status);
        $scope.Todo = null;
        Order.setActiveOrder(null);
        Alerts.setMessage(
            Alerts.getAlertTypes().alertType_Warning, 
            Alerts.getAlertTitles().alertTitle_Warning, 
            Alerts.getAlertMessages().alertMessage_WarningAlreadyRejected,         
            false);
      }
    };

    $scope.undoRejection = function(orderid) {
      $scope.updateOrderStatus(orderid, 1);
      $scope.Todo = null;
    };

    $scope.updateOrderStatus = function(orderid, status) {
        Order.updateOrderStatus(status, orderid, function() {
            Order.getFilteredLists();
        });
    };

  });
