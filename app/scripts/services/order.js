'use strict';

angular.module('yeahdoneit')
  .factory('Order', function ($resource, $http, Alerts, Styling, $filter) {

    return {

      Orders: null, 
      OrdersSuggested: null,
      OrdersApproved: null,
      OrdersOrdered: null,
      OrdersRejected: null,
      OrdersFavourited: null,
      ActiveOrder: null, 

      getPersonOrders: function(person) {
        var self = this;
        var promise = $http.get('/api/personorders/' + person);
        promise.then(function(response){
          self.Orders = response.data;
          // filter other lists
          self.getFilteredLists();        
          console.log("Orders", self.Orders);
        }).catch(function(e){
           throw e;
        }).then(function(res){
            // do more stuff
        }).catch(function(e){
            // handle errors in processing or in error.
           Alerts.setMessage(
                Alerts.getAlertTypes().alertType_Error, 
                Alerts.getAlertTitles().alertTitle_Error, 
                Alerts.getAlertMessages().alertMessage_GlobalTryAgain, 
                false);
        });  
      },

      getUserOrders: function(callback) {
        var self = this;
        var promise = $http.get('/api/userorders/');
        promise.then(function(response){
          self.Orders = response.data;
          // filter other lists
          self.getFilteredLists();   
          if (callback) callback();
        }).catch(function(e){
           throw e;
        }).then(function(res){
            // do more stuff
        }).catch(function(e){
            // handle errors in processing or in error.
           Alerts.setMessage(
                Alerts.getAlertTypes().alertType_Error, 
                Alerts.getAlertTitles().alertTitle_Error, 
                Alerts.getAlertMessages().alertMessage_GlobalTryAgain, 
                false);
           if (callback) callback();
        });  
      },

      getFilteredLists: function() {
          this.OrdersSuggested = $filter('filter')(this.Orders, { status: 1 });
          this.OrdersApproved = $filter('filter')(this.Orders, { status: 2 });
          this.OrdersOrdered = $filter('filter')(this.Orders, { status: 3 });
          this.OrdersRejected = $filter('filter')(this.Orders, { status: 0 });
          this.OrdersFavourited = $filter('filter')(this.Orders, { status: 9 });
      },
      
      createPersonOrder: function(order) {
        var self = this;
        var promise = $http.post('/api/order/', order);

        promise.then(function(response){
           Alerts.setMessage(
                Alerts.getAlertTypes().alertType_Success, 
                Alerts.getAlertTitles().alertTitle_Success, 
                Alerts.getAlertMessages().alertMessage_SuccessOrderStatusUpdated,
                true);
            self.Orders.splice(0, 0, order);
            // Add to relevant filtered list
            self.getFilteredLists();          
        }).catch(function(e){
           throw e;
        }).then(function(res){
            // do more stuff
        }).catch(function(e){
            // handle errors in processing or in error.
           Alerts.setMessage(
                Alerts.getAlertTypes().alertType_Error, 
                Alerts.getAlertTitles().alertTitle_Error, 
                Alerts.getAlertMessages().alertMessage_ErrorSavingOrder, 
                false);
        });  
      },

      deleteOrder: function (orderindex, id) {
        this.Orders.splice(orderindex, 1);        
        var self = this;
        var promise = $http.delete('/api/order/' + id);

        promise.then(function(response){
           Alerts.setMessage(
                Alerts.getAlertTypes().alertType_Success, 
                Alerts.getAlertTitles().alertTitle_Success, 
                Alerts.getAlertMessages().alertMessage_SuccessOrderRemoved,
                true);
            // Remove this item
            self.getFilteredLists();          
        }).catch(function(e){
           throw e;
        }).then(function(res){
            // do more stuff
        }).catch(function(e){
            // handle errors in processing or in error.
           Alerts.setMessage(
                Alerts.getAlertTypes().alertType_Error, 
                Alerts.getAlertTitles().alertTitle_Error, 
                Alerts.getAlertMessages().alertMessage_ErrorSavingOrder, 
                false);
        });          
      },

      remindSuggest: function (id) {
        var self = this;
        var promise = $http.post('/api/order/remind/' + id);

        promise.then(function(response){
           Alerts.setMessage(
                Alerts.getAlertTypes().alertType_Success, 
                Alerts.getAlertTitles().alertTitle_Success, 
                Alerts.getAlertMessages().alertMessage_SuccessOrderRemind,
                true);
            // Remove this item
            self.getFilteredLists();          
        }).catch(function(e){
           throw e;
        }).then(function(res){
            // do more stuff
        }).catch(function(e){
            // handle errors in processing or in error.
           Alerts.setMessage(
                Alerts.getAlertTypes().alertType_Error, 
                Alerts.getAlertTitles().alertTitle_Error, 
                Alerts.getAlertMessages().alertMessage_ErrorRemindOrder, 
                false);
        });          

      },

      clearOrders: function() {
        this.Orders = null;
        this.OrdersSuggested = null;
        this.OrdersApproved = null;
        this.OrdersOrdered = null;
        this.OrdersRejected = null;
      },

      updateOrderStatus: function (status, orderid, callback) {
        var self = this;
        
        // find order in list and update status
        var currentOrder = $filter('filter')(this.Orders, { _id: orderid })[0];
        var OrdersIndex = this.Orders.indexOf(currentOrder);
        this.Orders[OrdersIndex].status = status;

        // Build base order
        var thisOrder = {
          _id: orderid,
          status: status
        };

        var promise = $http.post('/api/order/' + orderid, thisOrder);
        console.log("promising,..."); 
        promise.then(function(response){
           Alerts.setMessage(
                Alerts.getAlertTypes().alertType_Success, 
                Alerts.getAlertTitles().alertTitle_Success, 
                Alerts.getAlertMessages().alertMessage_SuccessOrderStatusUpdated,
                true);
            self.Orders.splice(0, 0, response.data);
            if (callback) {
              callback();
            }
        }).catch(function(e){
           throw e;
        }).then(function(res){
            // do more stuff
        }).catch(function(e){
            // handle errors in processing or in error.
           Alerts.setMessage(
                Alerts.getAlertTypes().alertType_Error, 
                Alerts.getAlertTitles().alertTitle_Error, 
                Alerts.getAlertMessages().alertMessage_ErrorSavingAdate, 
                false);
            if (callback) {
              callback();
            }
        });  
      },

      setActiveOrder: function(orderid){
        if (orderid) {
          var activatedOrder = $filter('filter')(this.Orders, { _id: orderid });
          if (activatedOrder[0]) {
            this.ActiveOrder = activatedOrder[0];            
          } else {
            this.ActiveOrder = null;            
          }
        } else {
          console.log("setting to null in order.setActiveOrder");
          this.ActiveOrder = null;            
        }
      }
    }
  });
