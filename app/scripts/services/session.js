'use strict';

angular.module('yeahdoneit')
  .factory('Session', function ($resource) {
    return $resource('/api/session/');
  });
