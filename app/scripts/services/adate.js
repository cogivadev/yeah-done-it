'use strict';

angular.module('yeahdoneit')
  .factory('Adate', function ($resource, $http, Alerts, Styling, $filter) {

    return {

      Adates: null,
      AdatesNoAction: null,
      AdatesSuggested: null,
      AdatesApproved: null,
      TheAdate: null,
      
      getComingDates: function() {
        var self = this;
        var promise = $http.get('/api/comingadates');

        promise.then(function(response){
          self.Adates = response.data; 
          self.getFilteredLists();
          Styling.setLoading(false);
        }).catch(function(e){
           throw e;
        }).then(function(res){
            // do more stuff
        }).catch(function(e){
            // handle errors in processing or in error.
           Alerts.setMessage(
                Alerts.getAlertTypes().alertType_Error, 
                Alerts.getAlertTitles().alertTitle_Error, 
                Alerts.getAlertMessages().alertMessage_GlobalTryAgain, 
                false);
           Styling.setLoading(false);
        });  
      },

      getFilteredLists: function() {
          this.AdatesNoAction = $filter('filter')(this.Adates, { status: 0 });
          this.AdatesSuggested = $filter('filter')(this.Adates, { status: 1 });
          this.AdatesApproved = $filter('filter')(this.Adates, { status: 2 });
      },

      getPersonAdates: function(personid, callback) {
        var self = this;
        var promise = $http.get('/api/personadates/'+ personid);

        promise.then(function(response){
            self.Adates = response.data;
            if (callback){
              callback();              
            }
            Styling.setLoading(false);
        }).catch(function(e){
           throw e;
        }).then(function(res){
            // do more stuff
        }).catch(function(e){
            // handle errors in processing or in error.
           Alerts.setMessage(
                Alerts.getAlertTypes().alertType_Error, 
                Alerts.getAlertTitles().alertTitle_Error, 
                Alerts.getAlertMessages().alertMessage_ErrorGettingAdate, 
                false);
            if (callback){
              callback();              
            }
           Styling.setLoading(false);
        });          
      },

      getAdate: function(adateid){
        var self = this;
        var promise = $http.get('/api/adate/'+ adateid);

        promise.then(function(response){
          self.TheAdate = response.data; 
          Styling.setLoading(false);
        }).catch(function(e){
           throw e;
        }).then(function(res){
            // do more stuff
        }).catch(function(e){
            // handle errors in processing or in error.
           Alerts.setMessage(
                Alerts.getAlertTypes().alertType_Error, 
                Alerts.getAlertTitles().alertTitle_Error, 
                Alerts.getAlertMessages().alertMessage_GlobalTryAgain, 
                false);
           Styling.setLoading(false);
        });  
      },

      saveAdate: function(adate, adateid, callback){
        var promise = $http.put('/api/adate/'+ adateid, adate);
        console.log("Im saving...");
        promise.then(function(response){
           Alerts.setMessage(
                Alerts.getAlertTypes().alertType_Success, 
                Alerts.getAlertTitles().alertTitle_Success, 
                Alerts.getAlertMessages().alertMessage_SuccessPersonSaved,
                true);
           if (callback) {
              callback();
           }
           Styling.setLoading(false);
        }).catch(function(e){
           throw e;
        }).then(function(res){
            // do more stuff
        }).catch(function(e){
            // handle errors in processing or in error.
           Alerts.setMessage(
                Alerts.getAlertTypes().alertType_Error, 
                Alerts.getAlertTitles().alertTitle_Error, 
                Alerts.getAlertMessages().alertMessage_ErrorSavingPerson, 
                false);
           if (callback) {
              callback();
           }
           Styling.setLoading(false);
        });      
      },

      createAdate: function(adate, callback){
        var self = this;
        var promise = $http.post('/api/adate/', adate);

        promise.then(function(response){
           Alerts.setMessage(
                Alerts.getAlertTypes().alertType_Success, 
                Alerts.getAlertTitles().alertTitle_Success, 
                Alerts.getAlertMessages().alertMessage_SuccessAdateSaved,
                true);

            self.Adates.splice(0, 0, response.data);
            Styling.setLoading(false);
            if (callback) callback();
        }).catch(function(e){
           throw e;
        }).then(function(res){
            // do more stuff
        }).catch(function(e){
            // handle errors in processing or in error.
           Alerts.setMessage(
                Alerts.getAlertTypes().alertType_Error, 
                Alerts.getAlertTitles().alertTitle_Error, 
                Alerts.getAlertMessages().alertMessage_ErrorSavingAdate, 
                false);
           Styling.setLoading(false);
           if (callback) callback();
        });          
      },

      deleteAdate: function(deleteId, dateIndex){
        this.Adates.splice(dateIndex, 1);        
        
        var promise = $http.delete('/api/adate/' + deleteId);

        promise.then(function(response){
            Alerts.setMessage(
                Alerts.getAlertTypes().alertType_Success, 
                Alerts.getAlertTitles().alertTitle_Success, 
                Alerts.getAlertMessages().alertMessage_SuccessAdateDeleted,
                true);
        }).catch(function(e){
           throw e;
        }).then(function(res){
            // do more stuff
        }).catch(function(e){
            // handle errors in processing or in error.
            Alerts.setMessage(
                Alerts.getAlertTypes().alertType_Error, 
                Alerts.getAlertTitles().alertTitle_Error, 
                Alerts.getAlertMessages().alertMessage_ErrorDeletingAdate, 
                false);
        });         
      },

      clearAdate: function() {
        this.Adates = null;
        this.AdatesNoAction = null;
        this.AdatesSuggested = null;
        this.AdatesApproved = null;
      },

      clearTheAdate: function() {
        this.TheAdate = null;
      },

      updateStatus: function(status, callback) {
          var self = this;
          this.TheAdate.status = status;
          // change status on the adate in the orders list
          var AdatesIndex = this.Adates.indexOf(this.TheAdate);
          this.Adates[AdatesIndex].status = status;

          var newAdate = {
              status: status
          };

          this.saveAdate(newAdate, this.TheAdate._id, function() {
            callback();
            if (status === 1 || status == 2) self.TheAdate = null;
          });
      }

    }
  });
