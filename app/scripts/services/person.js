'use strict';

angular.module('yeahdoneit')
  .factory('Person', function ($resource, $http, Alerts, $timeout, Styling, $interval) {

    return { 

      Persons: null,
      ThePerson: null,

      getMyPersons: function(){
        var self = this;
        var promise = $http.get('/api/mypersons');

        promise.then(function(response){
          self.Persons = response.data;
          Styling.setLoading(false);
        }).catch(function(e){
           throw e;
        }).then(function(res){
            // do more stuff
        }).catch(function(e){
            // handle errors in processing or in error.
           Alerts.setMessage(
                Alerts.getAlertTypes().alertType_Error, 
                Alerts.getAlertTitles().alertTitle_Error, 
                Alerts.getAlertMessages().alertMessage_GlobalTryAgain, 
                false);
           Styling.setLoading(false);
        });  
      },
      
      getPerson: function(personid){
        return $http.get('/api/person/'+ personid);
      },

      savePerson: function(person, personid, callback){
        var self = this;        
        var promise = $http.put('/api/person/'+ personid, person);;
        promise.then(function(response){

           Alerts.setMessage(
                Alerts.getAlertTypes().alertType_Success, 
                Alerts.getAlertTitles().alertTitle_Success, 
                Alerts.getAlertMessages().alertMessage_SuccessPersonSaved,
                true);
            if (callback) callback();
        }).catch(function(e){
           throw e;
        }).then(function(res){ 
            // do more stuff
        }).catch(function(e){
            // handle errors in processing or in error.
           Alerts.setMessage(
                Alerts.getAlertTypes().alertType_Error, 
                Alerts.getAlertTitles().alertTitle_Error, 
                Alerts.getAlertMessages().alertMessage_ErrorSavingPerson, 
                false);
            if (callback) callback();
        });            
      },

      createPerson: function(person, callback){
        var self = this;
        var promise = $http.post('/api/person/', person);

        promise.then(function(response){
            Alerts.setMessage(
                Alerts.getAlertTypes().alertType_Success, 
                Alerts.getAlertTitles().alertTitle_Success, 
                Alerts.getAlertMessages().alertMessage_SuccessPersonSaved,
                true);
            self.Persons.splice(0,0,response.data);
            if (callback) callback(response.data);
        }).catch(function(e){
           throw e;
        }).then(function(res){
            // do more stuff
        }).catch(function(e){
            // handle errors in processing or in error.
           Alerts.setMessage(
                Alerts.getAlertTypes().alertType_Error, 
                Alerts.getAlertTitles().alertTitle_Error, 
                Alerts.getAlertMessages().alertMessage_ErrorSavingPerson, 
                false);
            if (callback) callback(null);
        });          
      },

      deletePerson: function(deleteIndex){
        var self = this;
        var deleteId =  self.Persons[deleteIndex]._id;
        self.Persons.splice(deleteIndex, 1);
        
        var promise = $http.delete('/api/person/' + deleteId);;

        promise.then(function(response){
            Alerts.setMessage(
                Alerts.getAlertTypes().alertType_Success, 
                Alerts.getAlertTitles().alertTitle_Success, 
                Alerts.getAlertMessages().alertMessage_SuccessPersonDeleted,
                true);
        }).catch(function(e){
           throw e;
        }).then(function(res){
            // do more stuff
        }).catch(function(e){
            // handle errors in processing or in error.
            Alerts.setMessage(
                Alerts.getAlertTypes().alertType_Error, 
                Alerts.getAlertTitles().alertTitle_Error, 
                Alerts.getAlertMessages().alertMessage_ErrorDeletingPerson, 
                false);
        });         
      },

      clearPerson: function() {
        console.log("clearing person object...");
        this.ThePerson = null;
      }
    }
  });
