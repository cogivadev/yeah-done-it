'use strict';

angular.module('yeahdoneit')
  .factory('Alerts', function ($timeout) {
  	
	var alertTitles = { 
		alertTitle_Error: 'Error!',
		alertTitle_Success: 'Hooray, We Did It!',
		alertTitle_Warning: 'Wo-ah! Hold your horses!'		
	}

	var alertMessages = {
  		alertMessage_GlobalTryAgain: " Please Try Again.  If the problem persists, please contact support.",
  		alertMessage_BugReportPosted: "Thanks for your help. Your bug report has been sent and we will get onto sorting it.",
  		alertMessage_MessagePosted: "Thanks for your Message. We've got it and we'll get back to you shortly.",
  		alertMessage_InviteCreated: "TThe new user invite has been created.",

  		// Person Messages
  		alertMessage_ErrorGettingPerson: "Unable to get the information for this person.",
  		alertMessage_ErrorGettingPersons: "Unable to get the information you people.",
  		alertMessage_ErrorSavingPerson: "Unable to save the information for this person.",
  		alertMessage_ErrorDeletingPerson: "Unable to remove the information for this person.",
  		alertMessage_PersonDataReceived: "Person found and loaded.",
  		alertMessage_SuccessPersonSaved: "Person Saved. Just click on the fields to edit - Hit enter to save!",
  		alertMessage_SuccessPersonDeleted: "Person removed.",

  		// Adate Messages
  		alertMessage_ErrorGettingAdate: "Unable to get the date information.",
  		alertMessage_ErrorSavingAdate: "Unable to save the date information.",
  		alertMessage_ErrorDeletingAdate: "Unable to remove the information for this date.",
  		alertMessage_AdateDataReceived: "Date found and loaded.",
  		alertMessage_SuccessAdateSaved: "Date Saved. Just click on the fields to edit - Hit enter to save!",
  		alertMessage_SuccessAdateDeleted: "Date removed.",

  		// Order Messages
  		alertMessage_SuccessOrderStatusUpdated: "Order status updated.",
  		alertMessage_SuccessOrderRemoved: "Order removed.",
  		alertMessage_SuccessOrderRemind: "Order suggestion reminder sent.",
  		alertMessage_ErrorSavingOrder: "Unable to change the order.",  		
  		alertMessage_ErrorRemindOrder: "Order suggestion reminder failed.",  
  		alertMessage_WarningAlreadyRejected: "This order has already been processed.",		
  		alertMessage_WarningAlreadyAccepted: "This order has already been processed.",
  		alertMessage_alertMessage_SuccessPayment: "Payment made successfully!",

  		// User messages
  		alertMessage_ChangePasswordSuccess: "Password successfully changed.",	
  		alertMessage_ChangePasswordIncorrect: "Old password incorrect. Try again!"	

  	};

	var alertTypes = {
		alertType_Success: "alert-success",
	  	alertType_Info: "alert-info",
	  	alertType_Warning: "alert-warning",
	  	alertType_Error: "alert-danger"
  	};
		
	var unsetPromise = null;

  	return {

		alertType: null,
		alertTitle: null,
		alertMessage: null,

		setMessage: function(type, title, message, timed) {		
        	var self = this;
		  	$timeout.cancel(unsetPromise);
		  	self.alertType = type;
		  	self.alertTitle = title;
		  	self.alertMessage = message;

		  	if (type == "alert-danger")
		  	{
		  		self.alertMessage = message; //alertMessages.alertMessage_GlobalTryAgain;
		  	}
		  	if (timed)
		  	{
		  		//unsetPromise = $timeout(self.unsetMessage,5000);
		  		unsetPromise = $timeout(function() {
			        self.unsetMessage.apply(self);
			    }, 2500);
		  	}
		},

		unsetMessage: function(self) {
			var me = (typeof self !== 'undefined' ? self : this);
		  	me.alertType = null;
		  	me.alertTitle = null;
		  	me.alertMessage = null;  	
		  	$timeout.cancel(unsetPromise);
		},

		getAlertTitles: function() {
			return alertTitles;
		},

		getAlertTypes: function() {
			return alertTypes;
		},

		getAlertMessages: function() {
			return alertMessages;
		}
	}
  });