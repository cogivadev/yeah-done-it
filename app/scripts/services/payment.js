'use strict';

angular.module('yeahdoneit')
  .factory('Payment', function ($resource, $http, Alerts, Styling, $filter) {

    return {

      chargeCard: function(orderid, stripetoken, callback) {
        var self = this;
        console.log("Calling payment...");
        var promise = $http.post('/api/chargecard/' + orderid, stripetoken);

        promise.then(function(response){
          console.log(response.data);
          Alerts.setMessage(
                Alerts.getAlertTypes().alertType_Success, 
                Alerts.getAlertTitles().alertTitle_Success, 
                Alerts.getAlertMessages().alertMessage_SuccessPayment,
                true);
          if (callback) callback(response.data);
          console.log("payment return",response.data);
        }).catch(function(e){
           throw e;
        }).then(function(res){
            // do more stuff
        }).catch(function(e){
            // handle errors in processing or in error.
           Alerts.setMessage(
                Alerts.getAlertTypes().alertType_Error, 
                Alerts.getAlertTitles().alertTitle_Error, 
                Alerts.getAlertMessages().alertMessage_GlobalTryAgain, 
                false);
          Styling.setAmazonLoading(false);
          if (callback) callback();
        });  
      }
    }
  });
