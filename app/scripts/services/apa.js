'use strict';

angular.module('yeahdoneit')
  .factory('AmazonAPI', function ($resource, $http, Alerts, Styling, $filter) {

    return {

      SearchItems: null, 

      search: function(searchterm, budget) {
        var self = this;
        var body = { budget: budget };
        var promise = $http.post('/api/amazonsearch/' + searchterm, body);

        promise.then(function(response){
          self.SearchItems = response.data;
          console.log(self.SearchItems);
          Styling.setAmazonLoading(false);
        }).catch(function(e){
           throw e;
        }).then(function(res){
            // do more stuff
        }).catch(function(e){
            // handle errors in processing or in error.
           Alerts.setMessage(
                Alerts.getAlertTypes().alertType_Error, 
                Alerts.getAlertTitles().alertTitle_Error, 
                Alerts.getAlertMessages().alertMessage_GlobalTryAgain, 
                false);
          Styling.setAmazonLoading(false);
        });  
      },

      clearsearch: function() {
        this.SearchItems = false;
      }
    }
  });
