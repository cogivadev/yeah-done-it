'use strict';

angular.module('yeahdoneit')
  .factory('Notifications', function ($resource, $http, Alerts) {

  	return {
  	  currentUnread: null,
      Notifications: null,

      decrement: function(callback) {
        var self = this;
        console.log("IN Decrement self.currentUnread",self.currentUnread);
        if (self.currentUnread && self.currentUnread > 0) {
          self.currentUnread--;
          console.log("self.currentUnread",self.currentUnread);
          callback();
        }
      },

      getMyNotifications: function(){
        var self = this;
        var promise = $http.get('/api/notifications');

        promise.then(function(response){
          self.Notifications = response.data;
          self.getUnreadCount();
          console.log("self.Notifications", self.Notifications);
        }).catch(function(e){
           throw e;
        }).then(function(res){
            // do more stuff
        }).catch(function(e){
            // handle errors in processing or in error.
           Alerts.setMessage(
                Alerts.getAlertTypes().alertType_Error, 
                Alerts.getAlertTitles().alertTitle_Error, 
                Alerts.getAlertMessages().alertMessage_GlobalTryAgain, 
                false);
        });  
      },

      getUnreadCount: function() {
        var self = this;
        var promise = $http.get('/api/notificationcount');

        promise.then(function(response){
          self.currentUnread = response.data; 
        }).catch(function(e){
           throw e;
        }).then(function(res){ 
            // do more stuff
        }).catch(function(e){
            // handle errors in processing or in error.
        });  
      },

      markAsRead: function(notificationid, callback) {
        var self = this;
        var promise = $http.get('/api/notifications/read/' + notificationid);

        promise.then(function(response){
          if (callback) { callback(); }
        }).catch(function(e){
           throw e;
        }).then(function(res){
            // do more stuff
        }).catch(function(e){
            // handle errors in processing or in error.
            Alerts.setMessage(
                Alerts.getAlertTypes().alertType_Error, 
                Alerts.getAlertTitles().alertTitle_Error, 
                Alerts.getAlertMessages().alertMessage_GlobalTryAgain, 
                false);
            if (callback) { callback(); }
        }); 
      }  		
	}
  });