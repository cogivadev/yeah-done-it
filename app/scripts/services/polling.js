'use strict';

angular.module('yeahdoneit')
  .factory('Polling', function ($resource, $http, Person, Adate, $timeout) {

    return { 

      personListPollTO: null,
      adateListPollTO: null,

      setPersonListPoll: function () {
        personListPollTO = $timeout(pollPersonList, 5000);
      },

      pollPersonList: function() {
        Person.getMyPersons();
      }
    }

  });
