'use strict';

angular.module('yeahdoneit')
  .factory('AdminUsers', function ($resource, $http, Alerts, $timeout, Styling, $interval) {

    return { 

      UserList: null,
      CurrentUser: null,

      getUsers: function(){
        var self = this;
        var promise = $http.get('/api/users/');
        promise.then(function(response){
          self.UserList = response.data;
          // filter other lists
          console.log("UserList", self.UserList);
        }).catch(function(e){
           throw e;
        }).then(function(res){
            // do more stuff
        }).catch(function(e){
            // handle errors in processing or in error.
           Alerts.setMessage(
                Alerts.getAlertTypes().alertType_Error, 
                Alerts.getAlertTitles().alertTitle_Error, 
                Alerts.getAlertMessages().alertMessage_GlobalTryAgain, 
                false);
        });         
      },

      sendInvite: function(email, callback) {
        console.log("AUS: sending..", email);
        var self = this;
        var thisBody = {
          email: email
        };
        var promise = $http.post('/api/sendinvite/', thisBody);
        promise.then(function(response){
          // filter other lists
          console.log(response.data);
           Alerts.setMessage(
                Alerts.getAlertTypes().alertType_Success, 
                Alerts.getAlertTitles().alertTitle_Success, 
                Alerts.getAlertMessages().alertMessage_InviteCreated, 
                false);
           if (callback) callback();
        }).catch(function(e){
           throw e;
        }).then(function(res){
            // do more stuff
        }).catch(function(e){
            // handle errors in processing or in error.
           Alerts.setMessage(
                Alerts.getAlertTypes().alertType_Error, 
                Alerts.getAlertTitles().alertTitle_Error, 
                Alerts.getAlertMessages().alertMessage_GlobalTryAgain, 
                false);
           if (callback) callback();
        });         
      }
    }

  });
