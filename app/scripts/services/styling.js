'use strict';

angular.module('yeahdoneit') 
  .factory('Styling', function ($location) {
  
    var title = 'Welcome to Yeah Done It!';
    var loading = false;
    var amazonloading = false;
    var showMenu = false;

    return {

      setBG: function(){
        // sET BODY BG
        if ($location.path() == "/" || $location.path() == "")
        {
          jQuery('body').addClass("homepage");
        }
        else
        {
          jQuery('body').removeClass("homepage");
        }    
      },

      title: function() { 
        return title; 
      },

      setTitle: function(newTitle) { 
        title = newTitle; 
      },

      isLoading: function() {
        return loading;
      },

      isAmazonLoading: function() {
        return amazonloading;
      },

      setLoading: function(load){
        loading  = load;
      },

      setAmazonLoading: function(load){
        amazonloading  = load;
      }       
    }
  });


